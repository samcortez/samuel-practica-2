<?php namespace App\Maps;
/*
 *
 */
define('SBP', 'static/');

trait ImportsMap {

  protected $items = [

    'main' => [
       'styles' => [
         SBP.'vendor/bootstrap/css/bootstrap.min.css',
         // SBP.'css/app.css'
         SBP.'css/app-styles.css'
       ],
       'scripts' => [
         [SBP.'vendor/jquery/jquery.min.js', true],
         [SBP.'vendor/bootstrap/bootstrap.bundle.min.js', true],
         // [SBP.'js/app.js', true],
        [SBP.'js/app-bundle.js', true],
       ]
     ],

    'homePage' => [
      'styles'  => [SBP.'css/home-styles.css'],
      'scripts' => [[SBP.'js/home-bundle.js', true]],
    ],

    'single' => [
      'styles'      => [],
      'scripts'     => []
    ],

    'simple' => [
      'styles'    => [SBP.'css/page-blank-styles.css'],
      'scripts'   => [[SBP.'js/page-blank-bundle.js', true]]
    ],

    # Para plantillas de paginas personalizadas
    'pages' => [
      'tienda' => [
        'path'      => 'pages-template/tienda.php',
        'styles'      => [SBP.'css/tienda-styles.css'],
        'scripts'     => [SBP.'js/tienda-bundle.js', true]
      ],
      'perfil' => [
        'path'      => 'pages-template/perfil.php',
        'styles'      => [SBP.'css/perfil-styles.css'],
        'scripts'     => [SBP.'js/perfil-bundle.js', true]
      ],
      'pedido__finalizado' => [
        'path'      => 'pages-template/pedido__finalizado.php',
        'styles'      => [SBP.'css/pedido__finalizado-styles.css'],
        'scripts'     => [SBP.'js/pedido__finalizado-bundle.js', true]
      ],
      'pedido__recibido' => [
        'path'      => 'pages-template/pedido__recibido.php',
        'styles'      => [SBP.'css/pedido__recibido-styles.css'],
        'scripts'     => [SBP.'js/pedido__recibido-bundle.js', true]
      ],
    ],

    'pageNotFound404' => [
      'styles'      => [],
      'scripts'     => []
    ],

    'WooCommerce' => [
      'Product' => [
        'styles'  => [],
        'scripts' => []
      ],
      'Shop' => [
        'styles'  => [],
        'scripts' => []
      ],
      'Checkout' => [
        'styles'  => [],
        'scripts' => []
      ],
      'Cart' => [
        'styles'  => [],
        'scripts' => []
      ],
      'AccoutPage' => [
        'styles'  => [],
        'scripts' => []
      ],
      'AccountEditPage' => [
        'styles'  => [],
        'scripts' => []
      ],
      'Filtered' => [
        'styles'  => [],
        'scripts' => []
      ],
      'OrderReceivedPage' => [
        'styles'  => [],
        'scripts' => []
      ],
      'ProductCategory' => [
        'styles'  => [],
        'scripts' => []
      ],
      'ProductTag' => [
        'styles'  => [],
        'scripts' => []
      ],
      'ProductTaxonomy' => [
        'styles'  => [],
        'scripts' => []
      ],
    ]

  ];

}
