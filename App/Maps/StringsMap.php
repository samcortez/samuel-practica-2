<?php namespace App\Maps;
/*
 *
 */
trait StringsMap {
  protected $items = [
    'texto_404_not_found' => [
      'label' => 'Mensaje de pagina no encontrada (404)',
      'default'   => "La pagina que usted intenta buscar no existe!",
      'section' => 'seccion_textos_generales',
      'polyGroup' => 'Nextscale - Otras configuraciones'
    ],
    'home_titulo_sobre_nosotros' => [
      'label' => 'Mensaje de pagina no encontrada (404)',
      'default'   => "La pagina que usted intenta buscar no existe!",
      'section' => 'seccion_textos_generales',
      'polyGroup' => 'Nextscale - Otras configuraciones'
    ],
    'home_categorias_title' => [
      'label' => 'Titulo de la categorias',
      'default'   => "Categorias",
      'section' => 'seccion_navbar',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'home_categorias_1' => [
      'label' => 'Texto 1 de la subcategorias',
      'default'   => "Alimentos",
      'section' => 'seccion_navbar',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'home_categorias_1_1' => [
      'label' => 'Texto 1 de la categorias',
      'default'   => "Bebidas",
      'section' => 'seccion_navbar',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'home_categorias_1_2' => [
      'label' => 'Texto 2 de la categorias',
      'default'   => "Café Te Infusiones",
      'section' => 'seccion_navbar',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'home_categorias_1_3' => [
      'label' => 'Texto 3 de la categorias',
      'default'   => "Especies",
      'section' => 'seccion_navbar',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'home_categorias_1_4' => [
      'label' => 'Texto 4 de la categorias',
      'default'   => "Cereales",
      'section' => 'seccion_navbar',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'home_categorias_1_5' => [
      'label' => 'Texto 5 de la categorias',
      'default'   => "Snack",
      'section' => 'seccion_navbar',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'home_categorias_1_6' => [
      'label' => 'Texto 6 de la categorias',
      'default'   => "Salsa",
      'section' => 'seccion_navbar',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'home_categorias_1_7' => [
      'label' => 'Texto 7 de la categorias',
      'default'   => "Alimentos Enlatados",
      'section' => 'seccion_navbar',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'home_categorias_1_8' => [
      'label' => 'Texto 8 de la categorias',
      'default'   => "Reposteria",
      'section' => 'seccion_navbar',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'home_categorias_1_9' => [
      'label' => 'Texto 9 de la categorias',
      'default'   => "Aceites y Vinagres",
      'section' => 'seccion_navbar',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'home_categorias_1_10' => [
      'label' => 'Texto 10 de la categorias',
      'default'   => "Mermeladas y gelatinas",
      'section' => 'seccion_navbar',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'home_categorias_1_11' => [
      'label' => 'Texto 11 de la categorias',
      'default'   => "Pasta y arroces",
      'section' => 'seccion_navbar',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'home_categorias_1_12' => [
      'label' => 'Texto 12 de la categorias',
      'default'   => "Untables",
      'section' => 'seccion_navbar',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'home_categorias_1_13' => [
      'label' => 'Texto 13 de la categorias',
      'default'   => "Endulzanbles",
      'section' => 'seccion_navbar',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'home_categorias_2' => [
      'label' => 'Texto 2 de la subcategorias',
      'default'   => "Charcuteria",
      'section' => 'seccion_navbar',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'home_categorias_3' => [
      'label' => 'Texto 3 de la subcategorias',
      'default'   => "Cofiteria",
      'section' => 'seccion_navbar',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'home_categorias_4' => [
      'label' => 'Texto 4 de la subcategorias',
      'default'   => "Cuidado del Bebé",
      'section' => 'seccion_navbar',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'home_categorias_5' => [
      'label' => 'Texto 5 de la subcategorias',
      'default'   => "Cuidado del Bebé",
      'section' => 'seccion_navbar',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'home_categorias_5_1' => [
      'label' => 'Texto 1 de la subcategorias',
      'default'   => "Protección Femenina",
      'section' => 'seccion_navbar',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'home_categorias_5_2' => [
      'label' => 'Texto 2 de la subcategorias',
      'default'   => "Desodorantes",
      'section' => 'seccion_navbar',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'home_categorias_5_3' => [
      'label' => 'Texto 3 de la subcategorias',
      'default'   => "Afeitados",
      'section' => 'seccion_navbar',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'home_categorias_5_4' => [
      'label' => 'Texto 4 de la subcategorias',
      'default'   => "Cuidado de la piel",
      'section' => 'seccion_navbar',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'home_categorias_5_5' => [
      'label' => 'Texto 5 de la subcategorias',
      'default'   => "Cuidado Bocal",
      'section' => 'seccion_navbar',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'home_categorias_5_6' => [
      'label' => 'Texto 6 de la subcategorias',
      'default'   => "Tuallitas humedas",
      'section' => 'seccion_navbar',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'home_categorias_5_7' => [
      'label' => 'Texto 7 de la subcategorias',
      'default'   => "Jabon de Tocador",
      'section' => 'seccion_navbar',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'home_categorias_5_8' => [
      'label' => 'Texto 8 de la subcategorias',
      'default'   => "Jabon Liquido",
      'section' => 'seccion_navbar',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'home_categorias_6' => [
      'label' => 'Texto 6 de la subcategorias',
      'default'   => "Limpieza",
      'section' => 'seccion_navbar',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'home_categorias_6_1' => [
      'label' => 'Texto 1 de la subcategorias',
      'default'   => "Detergente",
      'section' => 'seccion_navbar',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'home_categorias_6_2' => [
      'label' => 'Texto 2 de la subcategorias',
      'default'   => "Antibacteriales",
      'section' => 'seccion_navbar',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'home_categorias_6_3' => [
      'label' => 'Texto 3 de la subcategorias',
      'default'   => "Cloros",
      'section' => 'seccion_navbar',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'home_categorias_6_4' => [
      'label' => 'Texto 4 de la subcategorias',
      'default'   => "Limpia vidrios",
      'section' => 'seccion_navbar',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'home_categorias_7' => [
      'label' => 'Texto 7 de la subcategorias',
      'default'   => "Mascotas",
      'section' => 'seccion_navbar',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'home_categorias_7_1' => [
      'label' => 'Texto 1 de la subcategorias',
      'default'   => "Perros",
      'section' => 'seccion_navbar',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'home_categorias_7_2' => [
      'label' => 'Texto 2 de la subcategorias',
      'default'   => "Gatos",
      'section' => 'seccion_navbar',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'home_categorias_8' => [
      'label' => 'Texto 7 de la subcategorias',
      'default'   => "Papel",
      'section' => 'seccion_navbar',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'home_categorias_8_1' => [
      'label' => 'Texto 1 de la subcategorias',
      'default'   => "Papel toilet",
      'section' => 'seccion_navbar',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'home_categorias_8_2' => [
      'label' => 'Texto 2 de la subcategorias',
      'default'   => "Papel absorbente",
      'section' => 'seccion_navbar',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'home_btnnav_1' => [
      'label' => 'Boton Carrito',
      'default'   => "Carrito",
      'section' => 'seccion_navbar',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'home_btnnav_2' => [
      'label' => 'Boton Cuenta',
      'default'   => "Cuenta",
      'section' => 'seccion_navbar',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'home_movil_title' => [
      'label' => 'Titulo de Bienvenida',
      'default'   => "Hola",
      'section' => 'seccion_navbar',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'home_movil_texto' => [
      'label' => 'Texto de inciar sesión',
      'default'   => "Inicia sesión o crea tu cuenta para empezar a compara con unos cuantos clicks",
      'section' => 'seccion_navbar',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'home_movil_btn_1' => [
      'label' => 'Texto del Boton de inciar sesión',
      'default'   => "Inicia sesión",
      'section' => 'seccion_navbar',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'home_movil_btn_2' => [
      'label' => 'Texto del Boton de Crear una Cuenta',
      'default'   => "Crear una Cuenta",
      'section' => 'seccion_navbar',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'home_movil_menu_1' => [
      'label' => 'Menu Inicio',
      'default'   => "Inicio",
      'section' => 'seccion_navbar',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'home_movil_menu_2' => [
      'label' => 'Menu Categorias',
      'default'   => "Categorias",
      'section' => 'seccion_navbar',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'home_movil_menu_3' => [
      'label' => 'Menu Ayuda',
      'default'   => "Ayuda",
      'section' => 'seccion_navbar',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'home_movil_menu_4' => [
      'label' => 'Menu Contactanos',
      'default'   => "Contactanos",
      'section' => 'seccion_navbar',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'home_movil_menu_5' => [
      'label' => 'Menu Ubicanos',
      'default'   => "Ubicanos",
      'section' => 'seccion_navbar',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'title_portada' => [
      'label' => 'Titulo de la portada',
      'default'   => "¡Aprovecha nuestras ofertas semanales!",
      'section' => 'seccion_header',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'texto_portada' => [
      'label' => 'Texto de la portada',
      'default'   => "Tus productos favoritos los ponemos al alcance de tu mano en unos cuantos click",
      'section' => 'seccion_header',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'btn_portada' => [
      'label' => 'Boton de la portada',
      'default'   => "Ofertas Disponibles",
      'section' => 'seccion_header',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'title_section_1' => [
      'label' => 'Titulo de la seccion mejores categorias',
      'default'   => "Las mejores categorías",
      'section' => 'home_seccion_1',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'btn_section_1' => [
      'label' => 'Boton ver todas de la seccion mejores categorias',
      'default'   => "Ver todas",
      'section' => 'home_seccion_1',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'title_producto_1' => [
      'label' => 'Titulo del producto 1',
      'default'   => "Alimentos Secos",
      'section' => 'home_seccion_1',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'title_producto_2' => [
      'label' => 'Titulo del producto 2',
      'default'   => "Lácteos",
      'section' => 'home_seccion_1',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'title_producto_3' => [
      'label' => 'Titulo del producto 3',
      'default'   => "Dulces",
      'section' => 'home_seccion_1',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'title_producto_4' => [
      'label' => 'Titulo del producto 4',
      'default'   => "Verduras",
      'section' => 'home_seccion_1',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'title_producto_5' => [
      'label' => 'Titulo del producto 5',
      'default'   => "Frutas",
      'section' => 'home_seccion_1',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'title_producto_6' => [
      'label' => 'Titulo del producto 6',
      'default'   => "Licores",
      'section' => 'home_seccion_1',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'title_producto_7' => [
      'label' => 'Titulo del producto 7',
      'default'   => "Carnes",
      'section' => 'home_seccion_1',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],

    'title_producto_8' => [
      'label' => 'Titulo del producto 8',
      'default'   => "Bebidas",
      'section' => 'home_seccion_1',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'title_producto_9' => [
      'label' => 'Titulo del producto 9',
      'default'   => "Importados",
      'section' => 'home_seccion_1',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'title_producto_10' => [
      'label' => 'Titulo del producto 10',
      'default'   => "Reposterias",
      'section' => 'home_seccion_1',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'marcas_especiales_title' => [
      'label' => 'Titulo de las marcas especiales',
      'default'   => "Marcas Especiales para ti",
      'section' => 'seccion_marcas_especiales',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'marcas_especiales_btn' => [
      'label' => 'Boton ver todas',
      'default'   => "Ver todas",
      'section' => 'seccion_marcas_especiales',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'marcas_especiales_carrusel_title_1' => [
      'label' => 'Titulo de la marca del carrusel',
      'default'   => "Maizina America",
      'section' => 'seccion_marcas_especiales',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'marcas_especiales_carrusel_title_2' => [
      'label' => 'Titulo de la marca del carrusel',
      'default'   => "Kiero",
      'section' => 'seccion_marcas_especiales',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'marcas_especiales_carrusel_title_3' => [
      'label' => 'Titulo de la marca del carrusel',
      'default'   => "Naturalista",
      'section' => 'seccion_marcas_especiales',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'marcas_especiales_carrusel_btn' => [
      'label' => 'Boton ver todos los productos',
      'default'   => "Ver todos los productos",
      'section' => 'seccion_marcas_especiales',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'title_seccion_2' => [
      'label' => 'Titulo de la seccion 2',
      'default'   => "Productos destacados",
      'section' => 'seccion_carrusel',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'precio_carrusel_seccion_2' => [
      'label' => 'Precio del producto del carrusel',
      'default'   => "Bs. 250.000.00",
      'section' => 'seccion_carrusel',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'btn_carrusel_seccion_2' => [
      'label' => 'Boton del producto del carrusel',
      'default'   => "Agregar",
      'section' => 'seccion_carrusel',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'title_modal_carrusel_seccion_2' => [
      'label' => 'Titulo del modal del producto del carrusel',
      'default'   => "Maizina Americana",
      'section' => 'seccion_carrusel',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'description_modal_carrusel_seccion_2' => [
      'label' => 'descripción',
      'default'   => "Almidón de maíz o maicena, harina para hacer pan, pastas, bizcochos, bases de pizza.<br> Producto blanco, en polvo, muy suave y refinado, extraído de los mejores granos de maíz.",
      'section' => 'seccion_carrusel',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'detalles_modal_1' => [
      'label' => 'Unidad',
      'default'   => "Disponible 70 unidades",
      'section' => 'seccion_carrusel',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'detalles_modal_2' => [
      'label' => 'Codigo',
      'default'   => "SKU:PDD-22062020",
      'section' => 'seccion_carrusel',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'detalles_modal_3' => [
      'label' => 'Descripción 1',
      'default'   => "Another important info #1: Lorem",
      'section' => 'seccion_carrusel',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'detalles_modal_4' => [
      'label' => 'Descripción 2',
      'default'   => "Another important info #2: Lorem",
      'section' => 'seccion_carrusel',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'modal_precio' => [
      'label' => 'Precio del producto',
      'default'   => "Precio: 220,000.00 Bs",
      'section' => 'seccion_carrusel',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'btn_precio' => [
      'label' => 'Boton del producto',
      'default'   => "Agregar",
      'section' => 'seccion_carrusel',
      'polyGroup' => 'Nextscale - Home',
      'type' => 'textarea'
    ],
    'tienda_title' => [
      'label' => 'Titulo del menu marcas de tienda',
      'default'   => "Compra por marca",
      'section' => 'tienda_seccion_1',
      'polyGroup' => 'Nextscale - Tienda',
      'type' => 'textarea'
    ],
    'tienda_btn_menu_title' => [
      'label' => 'Boton del menu marcas de tienda',
      'default'   => "Elige una marca",
      'section' => 'tienda_seccion_1',
      'polyGroup' => 'Nextscale - Tienda',
      'type' => 'textarea'
    ],
    'tienda_menu_subtitle' => [
      'label' => 'Sub-titulo del menu',
      'default'   => "Comprar por categoria",
      'section' => 'tienda_seccion_1',
      'polyGroup' => 'Nextscale - Tienda',
      'type' => 'textarea'
    ],
    'tienda_categorias' => [
      'label' => 'Categoria',
      'default'   => "Categorias",
      'section' => 'tienda_seccion_1',
      'polyGroup' => 'Nextscale - Tienda',
      'type' => 'textarea'
    ],
    'tienda_subcategorias' => [
      'label' => 'Sub-categoria',
      'default'   => "Sub-categorias",
      'section' => 'tienda_seccion_1',
      'polyGroup' => 'Nextscale - Tienda',
      'type' => 'textarea'
    ],
    'tienda_btn_filtrar' => [
      'label' => 'Boton de busqueda',
      'default'   => "FILTRAR",
      'section' => 'tienda_seccion_1',
      'polyGroup' => 'Nextscale - Tienda',
      'type' => 'textarea'
    ],
    'tienda_title_carrusel' => [
      'label' => 'Titulo del Carrusel',
      'default'   => "Categoria #1",
      'section' => 'tienda_seccion_1',
      'polyGroup' => 'Nextscale - Tienda',
      'type' => 'textarea'
    ],
    'tienda_btn_carrusel_1' => [
      'label' => 'Boton de ver todos',
      'default'   => "Ver todos",
      'section' => 'tienda_seccion_1',
      'polyGroup' => 'Nextscale - Tienda',
      'type' => 'textarea'
    ],
    'tienda_title_carrusel_producto' => [
      'label' => 'Titulo del producto',
      'default'   => "Maizina 900grs",
      'section' => 'tienda_seccion_1',
      'polyGroup' => 'Nextscale - Tienda',
      'type' => 'textarea'
    ],
    'tienda_precio_carrusel_producto' => [
      'label' => 'Precio del producto',
      'default'   => "Bs. 250.000.00",
      'section' => 'tienda_seccion_1',
      'polyGroup' => 'Nextscale - Tienda',
      'type' => 'textarea'
    ],
    'tienda_btn_carrusel_producto' => [
      'label' => 'Boton de agregar del producto',
      'default'   => "Agregar",
      'section' => 'tienda_seccion_1',
      'polyGroup' => 'Nextscale - Tienda',
      'type' => 'textarea'
    ],
    'tienda_title_modal' => [
      'label' => 'Titulo del modal',
      'default'   => "Elige una marca para chequear los productos",
      'section' => 'tienda_seccion_1',
      'polyGroup' => 'Nextscale - Tienda',
      'type' => 'textarea'
    ],
    'tienda_subtitle_modal' => [
      'label' => 'Titulo del producto',
      'default'   => "Maizina Americana",
      'section' => 'tienda_seccion_1',
      'polyGroup' => 'Nextscale - Tienda',
      'type' => 'textarea'
    ],
    'tienda_title_modal_2' => [
      'label' => 'Titulo del producto',
      'default'   => "Maizina Americana",
      'section' => 'tienda_seccion_1',
      'polyGroup' => 'Nextscale - Tienda',
      'type' => 'textarea'
    ],
    'tienda_descripcion_modal_2' => [
      'label' => 'Descripción del producto',
      'default'   => "Almidón de maíz o maicena, harina para hacer pan, pastas, bizcochos, bases de pizza.<br> Producto blanco, en polvo, muy suave y refinado, extraído de los mejores granos de maíz.",
      'section' => 'tienda_seccion_1',
      'polyGroup' => 'Nextscale - Tienda',
      'type' => 'textarea'
    ],
    'tienda_detalles_1_modal_2' => [
      'label' => 'Unidad del producto',
      'default'   => "Disponible: 70 unidades",
      'section' => 'tienda_seccion_1',
      'polyGroup' => 'Nextscale - Tienda',
      'type' => 'textarea'
    ],
    'tienda_detalles_2_modal_2' => [
      'label' => 'Codigo del producto',
      'default'   => "SKU:PDD-22062020",
      'section' => 'tienda_seccion_1',
      'polyGroup' => 'Nextscale - Tienda',
      'type' => 'textarea'
    ],
    'tienda_detalles_3_modal_2' => [
      'label' => 'mini-descripción del producto',
      'default'   => "Another important info #1: Lorem",
      'section' => 'tienda_seccion_1',
      'polyGroup' => 'Nextscale - Tienda',
      'type' => 'textarea'
    ],
    'tienda_detalles_4_modal_2' => [
      'label' => 'mini-descripción 2 del producto',
      'default'   => "Another important info #2: Lorem",
      'section' => 'tienda_seccion_1',
      'polyGroup' => 'Nextscale - Tienda',
      'type' => 'textarea'
    ],
    'tienda_precios_modal_2' => [
      'label' => 'Precio del producto',
      'default'   => "Precio: 220,000.00 Bs",
      'section' => 'tienda_seccion_1',
      'polyGroup' => 'Nextscale - Tienda',
      'type' => 'textarea'
    ],
    'perfil_name' => [
      'label' => 'Nombre del Usuario',
      'default'   => "Nombre: Unnamed",
      'section' => 'perfil_seccion_1',
      'polyGroup' => 'Nextscale - Perfil',
      'type' => 'textarea'
    ],
    'perfil_rango' => [
      'label' => 'Rango del Usuario',
      'default'   => "Rango: S",
      'section' => 'perfil_seccion_1',
      'polyGroup' => 'Nextscale - Perfil',
      'type' => 'textarea'
    ],
    'perfil_inputs_1' => [
      'label' => 'Menu Escritorio',
      'default'   => "Escritorio",
      'section' => 'perfil_seccion_1',
      'polyGroup' => 'Nextscale - Perfil',
      'type' => 'textarea'
    ],
    'perfil_inputs_2' => [
      'label' => 'Menu Pedidos',
      'default'   => "Pedidos",
      'section' => 'perfil_seccion_1',
      'polyGroup' => 'Nextscale - Perfil',
      'type' => 'textarea'
    ],
    'perfil_inputs_3' => [
      'label' => 'Menu Direcciones',
      'default'   => "Direcciones",
      'section' => 'perfil_seccion_1',
      'polyGroup' => 'Nextscale - Perfil',
      'type' => 'textarea'
    ],
    'perfil_inputs_4' => [
      'label' => 'Menu Ajustes',
      'default'   => "Ajustes",
      'section' => 'perfil_seccion_1',
      'polyGroup' => 'Nextscale - Perfil',
      'type' => 'textarea'
    ],
    'perfil_inputs_5' => [
      'label' => 'Menu Ir a la tienda',
      'default'   => "Ir a la tienda",
      'section' => 'perfil_seccion_1',
      'polyGroup' => 'Nextscale - Perfil',
      'type' => 'textarea'
    ],
    'perfil_pedido_title' => [
      'label' => 'Titulo del pedido',
      'default'   => "Pedido #3465",
      'section' => 'perfil_seccion_1',
      'polyGroup' => 'Nextscale - Perfil',
      'type' => 'textarea'
    ],
    'perfil_pedido_fecha' => [
      'label' => 'Fecha del pedido',
      'default'   => "Se realizó el 1 julio, 2020 y está actualmente Procesando",
      'section' => 'perfil_seccion_1',
      'polyGroup' => 'Nextscale - Perfil',
      'type' => 'textarea'
    ],
    'perfil_pedido_subtitles_1' => [
      'label' => 'Sub-titulo del pedido',
      'default'   => "Producto",
      'section' => 'perfil_seccion_1',
      'polyGroup' => 'Nextscale - Perfil',
      'type' => 'textarea'
    ],
    'perfil_pedido_subtitles_2' => [
      'label' => 'Sub-titulo del Monto',
      'default'   => "Monto",
      'section' => 'perfil_seccion_1',
      'polyGroup' => 'Nextscale - Perfil',
      'type' => 'textarea'
    ],
    'perfil_pedido_subtitles_3' => [
      'label' => 'Sub-titulo del Dirección de facturación',
      'default'   => "Dirección de facturación",
      'section' => 'perfil_seccion_1',
      'polyGroup' => 'Nextscale - Perfil',
      'type' => 'textarea'
    ],
    'perfil_producto_1' => [
      'label' => 'Producto',
      'default'   => "Harina de maiz Doña Golla x 4",
      'section' => 'perfil_seccion_1',
      'polyGroup' => 'Nextscale - Perfil',
      'type' => 'textarea'
    ],
    'perfil_producto_2' => [
      'label' => 'Producto',
      'default'   => "Harina de trigo Doña Golla x 10",
      'section' => 'perfil_seccion_1',
      'polyGroup' => 'Nextscale - Perfil',
      'type' => 'textarea'
    ],
    'perfil_producto_precio_1' => [
      'label' => 'Precio',
      'default'   => "Bs 7.100.500",
      'section' => 'perfil_seccion_1',
      'polyGroup' => 'Nextscale - Perfil',
      'type' => 'textarea'
    ],
    'perfil_producto_precio_2' => [
      'label' => 'Precio',
      'default'   => "Bs 700.100.500",
      'section' => 'perfil_seccion_1',
      'polyGroup' => 'Nextscale - Perfil',
      'type' => 'textarea'
    ],
    'perfil_producto_name' => [
      'label' => 'Nombre',
      'default'   => "Eduardo Lara",
      'section' => 'perfil_seccion_1',
      'polyGroup' => 'Nextscale - Perfil',
      'type' => 'textarea'
    ],
    'perfil_producto_direccion' => [
      'label' => 'Direccion',
      'default'   => "Av, Libertador, 24 de Mayo",
      'section' => 'perfil_seccion_1',
      'polyGroup' => 'Nextscale - Perfil',
      'type' => 'textarea'
    ],
    'perfil_producto_codigo' => [
      'label' => 'Codigo',
      'default'   => "J-56445454-5",
      'section' => 'perfil_seccion_1',
      'polyGroup' => 'Nextscale - Perfil',
      'type' => 'textarea'
    ],
    'perfil_producto_tlf' => [
      'label' => 'Telefono',
      'default'   => "J+58 412 5478 421",
      'section' => 'perfil_seccion_1',
      'polyGroup' => 'Nextscale - Perfil',
      'type' => 'textarea'
    ],
    'perfil_producto_correo' => [
      'label' => 'Correo',
      'default'   => "eduedu@gmail.com",
      'section' => 'perfil_seccion_1',
      'polyGroup' => 'Nextscale - Perfil',
      'type' => 'textarea'
    ],
    'perfil_editar_title' => [
      'label' => 'Titulo de editar la  direccion del perfil',
      'default'   => "Aqui podras editar la dirección de facturación.",
      'section' => 'perfil_seccion_1',
      'polyGroup' => 'Nextscale - Perfil',
      'type' => 'textarea'
    ],
    'perfil_label_nombre' => [
      'label' => 'Nombre',
      'default'   => "Nombre",
      'section' => 'perfil_seccion_1',
      'polyGroup' => 'Nextscale - Perfil',
      'type' => 'textarea'
    ],
    'perfil_label_apellido' => [
      'label' => 'Apellido',
      'default'   => "Apellido",
      'section' => 'perfil_seccion_1',
      'polyGroup' => 'Nextscale - Perfil',
      'type' => 'textarea'
    ],
    'perfil_label_dirrecion' => [
      'label' => 'Empresa',
      'default'   => "Empresa",
      'section' => 'perfil_seccion_1',
      'polyGroup' => 'Nextscale - Perfil',
      'type' => 'textarea'
    ],
    'perfil_label_empresa' => [
      'label' => 'Dirreciones',
      'default'   => "Dirreciones",
      'section' => 'perfil_seccion_1',
      'polyGroup' => 'Nextscale - Perfil',
      'type' => 'textarea'
    ],
    'perfil_label_estado' => [
      'label' => 'Estados',
      'default'   => "Estados",
      'section' => 'perfil_seccion_1',
      'polyGroup' => 'Nextscale - Perfil',
      'type' => 'textarea'
    ],
    'perfil_label_ciudad' => [
      'label' => 'Ciudad',
      'default'   => "Ciudad",
      'section' => 'perfil_seccion_1',
      'polyGroup' => 'Nextscale - Perfil',
      'type' => 'textarea'
    ],
    'perfil_label_tlf' => [
      'label' => 'Teléfono',
      'default'   => "Teléfono",
      'section' => 'perfil_seccion_1',
      'polyGroup' => 'Nextscale - Perfil',
      'type' => 'textarea'
    ],
    'perfil_label_correo' => [
      'label' => 'Correo',
      'default'   => "Correo",
      'section' => 'perfil_seccion_1',
      'polyGroup' => 'Nextscale - Perfil',
      'type' => 'textarea'
    ],
    'perfil_editar_btn' => [
      'label' => 'Texto del boton',
      'default'   => "Regresar a la tienda",
      'section' => 'perfil_seccion_1',
      'polyGroup' => 'Nextscale - Perfil',
      'type' => 'textarea'
    ],
    'perfil_ajuste_title' => [
      'label' => 'Titulo de ajustes',
      'default'   => "Aqui podras cambiar ajustes de la cuenta.",
      'section' => 'perfil_seccion_1',
      'polyGroup' => 'Nextscale - Perfil',
      'type' => 'textarea'
    ],
    'perfil_subtitle_ajustes' => [
      'label' => 'Titulo Inferior de ajustes',
      'default'   => "Cambiar contraseña (Deja vacío en caso de no querer cambiarla)",
      'section' => 'perfil_seccion_1',
      'polyGroup' => 'Nextscale - Perfil',
      'type' => 'textarea'
    ],
    'perfil_label_contrasena_actual' => [
      'label' => 'Contrena actual',
      'default'   => "Contreña actual",
      'section' => 'perfil_seccion_1',
      'polyGroup' => 'Nextscale - Perfil',
      'type' => 'textarea'
    ],
    'perfil_label_contrasena_nueva' => [
      'label' => 'Contreseña nueva',
      'default'   => "Contreseña nueva",
      'section' => 'perfil_seccion_1',
      'polyGroup' => 'Nextscale - Perfil',
      'type' => 'textarea'
    ],
    'perfil_label_confirmar_contrasena' => [
      'label' => 'Confirmar Contraseña nueva',
      'default'   => "Confirmar Contraseña nueva",
      'section' => 'perfil_seccion_1',
      'polyGroup' => 'Nextscale - Perfil',
      'type' => 'textarea'
    ],
    'perfil_btn_cambiar_contrasena' => [
      'label' => 'Texto del Boton Cambiar contraseña',
      'default'   => "Cambiar contraseña",
      'section' => 'perfil_seccion_1',
      'polyGroup' => 'Nextscale - Perfil',
      'type' => 'textarea'
    ],
    'recibido_title' => [
      'label' => 'Titulo de la pagina recibido',
      'default'   => "Gracias!! Tu pedido ha sido recibido.",
      'section' => 'recibido_seccion_1',
      'polyGroup' => 'Nextscale - Recibido',
      'type' => 'textarea'
    ],
    'recibido_subtitle' => [
      'label' => 'Sub-titulo de recibido',
      'default'   => "Pronto nos podremos en contacto contigo.",
      'section' => 'recibido_seccion_1',
      'polyGroup' => 'Nextscale - Recibido',
      'type' => 'textarea'
    ],
    'recibido_facture_title' => [
      'label' => 'Titulo de la factura',
      'default'   => "Tu pedido",
      'section' => 'recibido_seccion_1',
      'polyGroup' => 'Nextscale - Recibido',
      'type' => 'textarea'
    ],
    'recibido_facture_producto_title' => [
      'label' => 'Sub-Titulo de la factura',
      'default'   => "Producto",
      'section' => 'recibido_seccion_1',
      'polyGroup' => 'Nextscale - Recibido',
      'type' => 'textarea'
    ],
    'recibido_facture_precio_title' => [
      'label' => 'Sub-Titulo de la factura',
      'default'   => "Subtotal",
      'section' => 'recibido_seccion_1',
      'polyGroup' => 'Nextscale - Recibido',
      'type' => 'textarea'
    ],
    'recibido_facture_producto_1' => [
      'label' => 'Producto 1 de la factura',
      'default'   => "Maiz de cotufa x2",
      'section' => 'recibido_seccion_1',
      'polyGroup' => 'Nextscale - Recibido',
      'type' => 'textarea'
    ],
    'recibido_facture_producto_2' => [
      'label' => 'Producto 2 de la factura',
      'default'   => "Harina de maiz x7",
      'section' => 'recibido_seccion_1',
      'polyGroup' => 'Nextscale - Recibido',
      'type' => 'textarea'
    ],
    'recibido_facture_producto_3' => [
      'label' => 'Producto 3 de la factura',
      'default'   => "Azucar morena x3",
      'section' => 'recibido_seccion_1',
      'polyGroup' => 'Nextscale - Recibido',
      'type' => 'textarea'
    ],
    'recibido_facture_precio_1' => [
      'label' => 'Precio 1 de la factura',
      'default'   => "Bs. 700",
      'section' => 'recibido_seccion_1',
      'polyGroup' => 'Nextscale - Recibido',
      'type' => 'textarea'
    ],
    'recibido_facture_precio_2' => [
      'label' => 'Precio 2 de la factura',
      'default'   => "Bs. 700.700",
      'section' => 'recibido_seccion_1',
      'polyGroup' => 'Nextscale - Recibido',
      'type' => 'textarea'
    ],
    'recibido_facture_precio_3' => [
      'label' => 'Precio 3 de la factura',
      'default'   => "Bs. 714.145.00",
      'section' => 'recibido_seccion_1',
      'polyGroup' => 'Nextscale - Recibido',
      'type' => 'textarea'
    ],
    'recibido_btn' => [
      'label' => 'Texto del Boton regresar',
      'default'   => "Regresar a la tienda",
      'section' => 'recibido_seccion_1',
      'polyGroup' => 'Nextscale - Recibido',
      'type' => 'textarea'
    ],
    'finalizado_title' => [
      'label' => 'Titulo de la pagina pedido finalizado',
      'default'   => "Finaliza tu pedido",
      'section' => 'finalizado_seccion_1',
      'polyGroup' => 'Nextscale - Recibido',
      'type' => 'textarea'
    ],
    'finalizado_subtitle_1' => [
      'label' => 'Sub-Titulo de la pagina pedido finalizado',
      'default'   => "Detalles de facturación",
      'section' => 'finalizado_seccion_1',
      'polyGroup' => 'Nextscale - Recibido',
      'type' => 'textarea'
    ],
    'finalizado_subtitle_2' => [
      'label' => 'Sub-Titulo de la pagina pedido finalizado',
      'default'   => "¿Tienes un cupon?",
      'section' => 'finalizado_seccion_1',
      'polyGroup' => 'Nextscale - Recibido',
      'type' => 'textarea'
    ],
    'finalizado_subtitle_3' => [
      'label' => 'Sub-Titulo de la pagina pedido finalizado',
      'default'   => "Información adicional",
      'section' => 'finalizado_seccion_1',
      'polyGroup' => 'Nextscale - Recibido',
      'type' => 'textarea'
    ],
    'finalizado_label_1' => [
      'label' => 'Nombre',
      'default'   => "Nombre",
      'section' => 'finalizado_seccion_1',
      'polyGroup' => 'Nextscale - Recibido',
      'type' => 'textarea'
    ],
    'finalizado_label_2' => [
      'label' => 'Apellido',
      'default'   => "Apellido",
      'section' => 'finalizado_seccion_1',
      'polyGroup' => 'Nextscale - Recibido',
      'type' => 'textarea'
    ],
    'finalizado_label_3' => [
      'label' => 'Nombre de la empresa',
      'default'   => "Nombre de la empresa",
      'section' => 'finalizado_seccion_1',
      'polyGroup' => 'Nextscale - Recibido',
      'type' => 'textarea'
    ],
    'finalizado_label_4' => [
      'label' => 'Dirección',
      'default'   => "Dirección",
      'section' => 'finalizado_seccion_1',
      'polyGroup' => 'Nextscale - Recibido',
      'type' => 'textarea'
    ],
    'finalizado_label_5' => [
      'label' => 'Estado',
      'default'   => "Estado",
      'section' => 'finalizado_seccion_1',
      'polyGroup' => 'Nextscale - Recibido',
      'type' => 'textarea'
    ],
    'finalizado_label_6' => [
      'label' => 'Ciudad',
      'default'   => "Ciudad",
      'section' => 'finalizado_seccion_1',
      'polyGroup' => 'Nextscale - Recibido',
      'type' => 'textarea'
    ],
    'finalizado_label_7' => [
      'label' => 'Teléfono',
      'default'   => "Teléfono",
      'section' => 'finalizado_seccion_1',
      'polyGroup' => 'Nextscale - Recibido',
      'type' => 'textarea'
    ],
    'finalizado_label_8' => [
      'label' => 'Correo electrónico',
      'default'   => "Correo electrónico",
      'section' => 'finalizado_seccion_1',
      'polyGroup' => 'Nextscale - Recibido',
      'type' => 'textarea'
    ],
    'finalizado_label_9' => [
      'label' => 'Notas del pedido (Opcional)',
      'default'   => "Notas del pedido (Opcional)",
      'section' => 'finalizado_seccion_1',
      'polyGroup' => 'Nextscale - Recibido',
      'type' => 'textarea'
    ],
    'finalizado_texto_naranja' => [
      'label' => 'Texto Naranja Canjealo',
      'default'   => "Canjealo",
      'section' => 'finalizado_seccion_1',
      'polyGroup' => 'Nextscale - Recibido',
      'type' => 'textarea'
    ],
    'finalizado_btn_1' => [
      'label' => 'Texto Boton Canjear',
      'default'   => "Canjear",
      'section' => 'finalizado_seccion_1',
      'polyGroup' => 'Nextscale - Recibido',
      'type' => 'textarea'
    ],
    'finalizado_btn_2' => [
      'label' => 'Texto Boton Guardar dirección',
      'default'   => "Guardar dirección",
      'section' => 'finalizado_seccion_1',
      'polyGroup' => 'Nextscale - Recibido',
      'type' => 'textarea'
    ],
    'title_footer' => [
      'label' => 'Titulo del footer',
      'default'   => "¡Hola! ¿en qué podemos ayudarte?",
      'section' => 'seccion_footer',
      'polyGroup' => 'Nextscale - Footer',
      'type' => 'textarea'
    ],
    'btn_footer' => [
      'label' => 'Texto Boton del footer',
      'default'   => "Enviar Mensaje",
      'section' => 'seccion_footer',
      'polyGroup' => 'Nextscale - Footer',
      'type' => 'textarea'
    ],
    'subtitle_footer_1' => [
      'label' => 'subtitle de footer',
      'default'   => "¡Conoce más de nosotros!",
      'section' => 'seccion_footer',
      'polyGroup' => 'Nextscale - Footer',
      'type' => 'textarea'
    ],
    'subtitle_footer_2' => [
      'label' => 'subtitle de footer',
      'default'   => "Ubicanos en...",
      'section' => 'seccion_footer',
      'polyGroup' => 'Nextscale - Footer',
      'type' => 'textarea'
    ],
    'link_footer_1' => [
      'label' => 'texto del link del footer',
      'default'   => "Historias",
      'section' => 'seccion_footer',
      'polyGroup' => 'Nextscale - Footer',
      'type' => 'textarea'
    ],
    'link_footer_2' => [
      'label' => 'texto del link del footer',
      'default'   => "Filosofía de trabajo",
      'section' => 'seccion_footer',
      'polyGroup' => 'Nextscale - Footer',
      'type' => 'textarea'
    ],
    'link_footer_3' => [
      'label' => 'texto del link del footer',
      'default'   => "Información para el negocios",
      'section' => 'seccion_footer',
      'polyGroup' => 'Nextscale - Footer',
      'type' => 'textarea'
    ],
    'footer_ubicacion_texto' => [
      'label' => 'Texto de la Ubicaion',
      'default'   => "Av. Principal Local/Galpón 280801 Sector Parque Industrial Los Pinos.
                Puerto Ordaz, Ciudad Guayana - Edo.Bolivar",
      'section' => 'seccion_footer',
      'polyGroup' => 'Nextscale - Footer',
      'type' => 'textarea'
    ],
    'numero_footer' => [
      'label' => 'Numero de contacto',
      'default'   => "No. de Contacto: +58 0286-994 8711",
      'section' => 'seccion_footer',
      'polyGroup' => 'Nextscale - Footer',
      'type' => 'textarea'
    ],
  ];
}
