<?php namespace App\Maps;
/*
 *
 */
trait SettingsMap {
  // Los items son "paneles"
  protected $items = [

      # Panel del Layout
      'panel_layout' => [
        'title'         => 'Layout',
        'description'   => 'Configuraciones del layout',
        'priority'      => 1,
        'sections' => [
            'seccion_navbar' => [
              'title'         =>  'Barra de navegacion',
              'description'   =>  'Barra de navegacion de la pagina',
              'priority'      =>  1,
              'inputs'        => [
                  'navbar_logo' => [
                    'title'   => 'Logo del navbar',
                    'type'    => 'img',
                    'default' => 'logo.png'
                  ],
                  'icono_search' => [
                    'title'   => 'Lupa de busqueda',
                    'type'    => 'img',
                    'default' => 'lupa.png'
                  ],
                  'icono_persona' => [
                    'title'   => 'Usuario',
                    'type'    => 'img',
                    'default' => 'person.png'
                  ]
              ]
            ],
            'seccion_footer' => [
              'title'         =>  'Footer',
              'description'   =>  'Footer de la pagina',
              'priority'      =>  2,
              'inputs'        => [
                  'footer_fondo' => [
                    'title'   => 'Fondo del footer',
                    'type'    => 'img',
                    'default' => 'contact.png'
                  ],
                  'footer_icon_nexscale' => [
                    'title'   => 'Icono de Instagram',
                    'type'    => 'image',
                    'default' => 'logo-blanco.png'
                  ],
                  'footer_icon_lidera' => [
                    'title'   => 'Icono de Instagram',
                    'type'    => 'image',
                    'default' => 'logo-blanco-lidera.png'
                  ],
              ]
            ]
        ]
    ],
    'panel_homepage' => [
        'title'         => 'Homepage',
        'description'   => 'Configuraciones de la homepage',
        'priority'      => 1,
        'sections' => [
            'seccion_header' => [
              'title'         =>  'Header',
              'description'   =>  'Header de la pagina',
              'priority'      =>  1,
              'inputs'        => [
                  'home_fondo_portada' => [
                    'title'   => 'Imagen de Fondo',
                    'type'    => 'img',
                    'default' => 'bg-home.png'
                  ],
              ]
            ],
            'home_seccion_1' => [
              'title'         =>  'Seccion 1',
              'description'   =>  'Seccion 1 de la pagina',
              'priority'      =>  2,
              'inputs'        => [
                  'home_seccion_1_corazones' => [
                    'title'   => 'Imagen',
                    'type'    => 'img',
                    'default' => 'globos-3.jpg'
                  ],
                  'home_seccion_1_ver_mas' => [
                    'title'   => 'Boton de Ver Mas',
                    'type'    => 'img',
                    'default' => 'Icono-Boton.svg'
                  ]
              ]
            ],
            'seccion_marcas_especiales' => [
              'title'         =>  'Marcas Especiales',
              'description'   =>  'Seccion de Marcas Especiales',
              'priority'      =>  3,
              'inputs'        => [
                  'imagen_marca_del_producto' => [
                    'title'   => 'Imagen del producto',
                    'type'    => 'image',
                    'default' => 'Maizina-Americana.png'
                  ],
                  'imagen_del_producto_1' => [
                    'title'   => 'Imagen por defecto',
                    'type'    => 'image',
                    'default' => 'ImgP1.png'
                  ],
                  'imagen_del_producto_2' => [
                    'title'   => 'Imagen por defecto',
                    'type'    => 'image',
                    'default' => 'ImgP2.png'
                  ]
              ]
            ],
            'seccion_carrusel' => [
              'title'         =>  'Carrusel',
              'description'   =>  'Seccion del Carrusel de la pagina',
              'priority'      =>  4,
              'inputs'        => [
                  'home_carrusel_producto_destacados' => [
                    'title'   => 'Maizina 900grs',
                    'type'    => 'image',
                    'default' => 'ImgP2.png'
                  ],
              ]
            ],
            'seccion_galeria' => [
              'title'         =>  'Galeria',
              'description'   =>  'Seccion de la Galeria de la pagina',
              'priority'      =>  5,
              'inputs'        => [
                  'url_galeria' => [
                    'title'   => 'Url base de galeria',
                    'type'    => 'url',
                    'default' => 'galeria'
                  ],
              ]
            ],
            'tienda_seccion_1' => [
              'title'         =>  'Seccion 1',
              'description'   =>  'Seccion 1 de la pagina tienda',
              'priority'      =>  2,
              'inputs'        => [
                  'imagen_del_producto_1' => [
                    'title'   => 'Imagen por defecto',
                    'type'    => 'image',
                    'default' => 'ImgP1.png'
                  ],
                  'imagen_del_producto_2' => [
                    'title'   => 'Imagen por defecto',
                    'type'    => 'image',
                    'default' => 'ImgP2.png'
                  ]
              ]
            ],
            'perfil_seccion_1' => [
              'title'         =>  'Seccion 1',
              'description'   =>  'Seccion 1 de la pagina perfil',
              'priority'      =>  3,
              'inputs'        => [
                'perfil_icono_persona' => [
                    'title'   => 'Usuario',
                    'type'    => 'img',
                    'default' => 'person.png'
                  ],
              ]
            ],
            'recibido_seccion_1' => [
              'title'         =>  'Seccion 1',
              'description'   =>  'Seccion 1 de la pagina de pedido recibido',
              'priority'      =>  3,
              'inputs'        => [
              ]
            ],
            'finalizado_seccion_1' => [
              'title'         =>  'Seccion 1',
              'description'   =>  'Seccion 1 de la pagina pedido finalizado',
              'priority'      =>  3,
              'inputs'        => [
              ]
            ],
        ]
    ],
    # Otros ajustes de plantilla
    'panel_others' => [
      'title'         => 'Otros ajustes',
      'description'   => 'Otros ajustes generales de plantilla',
      'priority'      => 1,
      'sections' => [
          'seccion_textos_generales' => [
            'title'         =>  'Textos generales',
            'description'   =>  'Textos generales de plantilla',
            'priority'      =>  1,
            'inputs'        => [
              // Solo tendra textos y estos se registraran en los StringsMap.php
            ]
          ],
          'seccion_imagenes_generales' => [
            'title'         =>  'Imagenes generales',
            'description'   =>  'Imagenes generales de la pagina',
            'priority'      =>  2,
            'inputs'        => [
              'favicon_page' => [
                'title'   => 'Imagen de favicon',
                'type'    => 'image',
                'default' => 'favicon.png'
              ],
            ]
          ],
      ]
    ],

  ];
}
