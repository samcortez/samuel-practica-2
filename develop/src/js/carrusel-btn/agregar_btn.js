var agregar = document.getElementsByClassName('carrusel__btn_agregar');
var salir = document.getElementsByClassName('carrusel__btn_salir');
var sumar = document.getElementsByClassName('carrusel__btn_sumar');
var restar = document.getElementsByClassName('carrusel__btn_restar');
var contador = [];
for (var i = 0; i <= agregar.length - 1; i++) {
    //agregar y salir del boton (agregar)
    agregar[i].addEventListener('click', (function(event) {
        this.classList.toggle("carrusel__btn-show");
        this.nextElementSibling.classList.toggle("carrusel__btn-show");
    }));
    salir[i].addEventListener('click', (function(event) {
        this.parentNode.classList.toggle("carrusel__btn-show");
        this.parentNode.previousElementSibling.classList.toggle("carrusel__btn-show");
    }));
    //sumar y restar el producto
    sumar[i].addEventListener('click', (function(event) {
        var posicion = indexInOf(this.previousElementSibling.parentNode.parentNode.parentNode);
        contador[posicion] = contador[posicion] + 1;
        this.previousElementSibling.innerHTML = contador[posicion];
    }));
    restar[i].addEventListener('click', (function(event) {
        var posicion = indexInOf(this.previousElementSibling.parentNode.parentNode.parentNode);
        contador[posicion] = contador[posicion] - 1;
        if (contador[posicion] < 1) {
            contador[posicion] = 1;
            this.nextElementSibling.innerHTML = contador[posicion];
        } else {
            this.nextElementSibling.innerHTML = contador[posicion];
        }
    }));
    contador.push(1);
};

function indexInOf(node) {
    var children = node.parentNode.childNodes;
    var num = 0;
    for (var i = 0; i < children.length; i++) {
        if (children[i] == node) return num;
        if (children[i].nodeType == 1) num++;
    }
    return -1;
}