const path = require('path');
// variable para cambiar la ruta de salida
var isProduction = true;
const production = (isProduction) ? '../../' : '';
const production_folder = (isProduction) ? '../../static/' : '';
const direction = (isProduction) ? '../' : '../static/';
// Plugins
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
module.exports = {
    entry: {
        app: './src/app.js',
        home: './src/js/home.js',
        tienda: './src/js/tienda.js',
        pedido__finalizado: './src/js/pedido__finalizado.js',
        pedido__recibido: './src/js/pedido__recibido.js',
        perfil: './src/js/perfil.js',
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: production_folder + 'js/[name]-bundle.js',
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './src/view/homepage.handlebars',
            filename: 'home.html',
            chunks: ['app', 'home']
        }),
        new HtmlWebpackPlugin({
            template: './src/view/tienda.handlebars',
            filename: 'tienda.html',
            chunks: ['app', 'tienda']
        }),
        new HtmlWebpackPlugin({
            template: './src/view/pedido__finalizado.handlebars',
            filename: 'pedido__finalizado.html',
            chunks: ['app', 'pedido__finalizado']
        }),
        new HtmlWebpackPlugin({
            template: './src/view/pedido__recibido.handlebars',
            filename: 'pedido__recibido.html',
            chunks: ['app', 'pedido__recibido']
        }),
        new HtmlWebpackPlugin({
            template: './src/view/perfil.handlebars',
            filename: 'perfil.html',
            chunks: ['app', 'perfil']
        }),
        new MiniCssExtractPlugin({
            filename: production_folder + 'css/[name]-styles.css',
        })
    ],
    module: {
        rules: [{
            test: /\.(sa|sc|c)ss$/,
            use: [
                MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader'
            ]
        }, {
            test: /\.handlebars$/,
            loader: 'handlebars-loader'
        }, {
            test: /\.(jpg|png|gif|svg|jpeg)$/,
            use: [{
                loader: 'file-loader',
                options: {
                    name: '[name].[ext]',
                    outputPath: production + 'static/img',
                    publicPath: direction + 'img',
                    useRelativePath: false,
                }
            }]
        }, {
            test: /\.(ttf|otf|woff2)$/,
            use: [{
                loader: 'file-loader',
                options: {
                    name: '[name].[ext]',
                    outputPath: production + 'static/fonts',
                    publicPath: direction + 'fonts',
                    useRelativePath: false,
                }
            }]
        }, {
            loader: 'image-webpack-loader', // Minificador de imagenes
            options: {
                mozjpeg: {
                    progressive: true,
                    quality: 65
                },
                // optipng.enabled: false will disable optipng
                optipng: {
                    enabled: true,
                },
                pngquant: {
                    quality: [0.65, 0.90],
                    speed: 4
                },
                gifsicle: {
                    interlaced: false,
                },
                // the webp option will enable WEBP
                webp: {
                    quality: 75
                }
            }
        }]
    }
};