<?php /* Template Name: Pagina de pedido recibido */ ?>
<?php /* Template description: Para jugar con elementor */ ?>
<?php use Opalo\Helpers\InPage; ?>
<?php get_header(); ?>

   <div class="recibido__bg">
      <div class="container__lidera">
      <h2 class="text-center recibido__title mb-0 pt-3"><?=InPage::__('recibido_title','Gracias!! Tu pedido ha sido recibido.')?>
</h2>
        <h3 class="text-center recibido__subtitle_orange"><?=InPage::__('recibido_subtitle','Pronto nos podremos en contacto contigo.')?></h3>
        </div>
    	<div class="container__lidera recibido__container">
        
        <div class="d-flex flex-wrap">

          <div class="col-12  mb-4">
              <div class="card recibido__radius">        
                <!--Card content-->
                <div class="recibido__box_precio">
                  <!--Title-->
                  <h4 class="recibido__subtitle text-left mb-4" ><?=InPage::__('recibido_facture_title','Tu pedido')?></h4>
                  <!--Text-->
                  <div class="d-flex justify-content-between">
                    <h5 class="recibido__subtitle_precio"><?=InPage::__('recibido_facture_producto_title','Producto')?></h5>
                    <h5 class="recibido__subtitle_precio"><?=InPage::__('recibido_facture_precio_title','Subtotal')?></h5>
                  </div>
                  <hr class="mt-0 recibido__line_gris" >
                  <div class="d-flex justify-content-between">
                    <h6 class="recibido__productos_font"><?=InPage::__('recibido_facture_producto_1','Maiz de cotufa x2')?></h6>
                    <h6 class="recibido__productos_font"><?=InPage::__('recibido_facture_precio_1','Bs. 700')?></h6>
                  </div>
                  <hr class="my-0 recibido__line_orange" >
                  <div class="d-flex justify-content-between">
                    <h6 class="recibido__productos_font"><?=InPage::__('recibido_facture_producto_2','Harina de maiz x7')?></h6>
                    <h6 class="recibido__productos_font"><?=InPage::__('recibido_facture_precio_2','Bs. 700.700')?></h6>
                  </div>
                  <hr class="my-0 recibido__line_orange" >
                  <div class="d-flex justify-content-between">
                    <h6 class="recibido__productos_font"><?=InPage::__('recibido_facture_producto_3','Azucar morena x3')?></h6>
                    <h6 class="recibido__productos_font"><?=InPage::__('recibido_facture_precio_3','Bs. 714.145.00')?></h6>
                  </div>
                  <div class="d-flex flex-column" style="min-height:200px">
                    <div class="mt-auto">
                      <hr class="my-0 recibido__line_gris" >
                  <div class="d-flex justify-content-between ">
                    <h6 class="text-left recibido__precios-text" ><?=InPage::__('recibido_facture_precio_title','Subtotal')?></h6>
                    <h6 class="text-right recibido__precios-text"><?=InPage::__('recibido_facture_producto_title','Bs. 500')?></h6>
                  </div>
                  <hr class="my-0 recibido__line_gris" >

                 
                    </div>
                  </div>
                  
                </div>

              </div>
          </div>
           <a href="#" class="recibido__btn_regresar mt-2 "><?=InPage::__('recibido_btn','Regresar a la tienda')?></a>
        </div>
      </div>
    </div>


<?php get_footer();?>
