<?php /* Template Name: Tienda */ ?>
<?php /* Template description: Para jugar con elementor */ ?>
<?php use Opalo\Helpers\InPage; ?>
<?php get_header(); ?>

<div class="lista__bg">
      	<div class="lista__col_marca d-flex flex-column lista__show-marca-movil lista__overflow" id="box_filtrar">
      		<a href="#salir_tu_casa" class="ml-auto lista__salir-x" id="salir"><i class="fas fa-times"></i></a>
      				<h5 class="lista__font"><?=InPage::__('tienda_title','Compra por marca')?></h5>
      				<a href="#" class="lista__btn" data-toggle="modal" data-target="#select_marca"><i class="fab fa-the-red-yeti lista__yeti"></i><br>
      				<?=InPage::__('tienda_btn_menu_title','Elige una marca')?></a>
      				<h5 class="lista__font mt-5"><?=InPage::__('tienda_menu_subtitle','Comprar por categoria')?></h5>
      					<div class="list-group">

      					  	<a href="#!" class="list-group-item list-group-item-action lista__link"><i class="fas fa-circle"></i> <?=InPage::__('tienda_categorias','Categorias')?></a>

      						  <div class="list-group lista__group2 lista__acordion">
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  </div>

      						<a href="#!" class="list-group-item list-group-item-action lista__link"><i class="fas fa-circle"></i> <?=InPage::__('tienda_categorias','Categorias')?></a>

      						  <div class="list-group lista__group2 lista__acordion">
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  </div>

      						<a href="#!" class="list-group-item list-group-item-action lista__link"><i class="fas fa-circle"></i> <?=InPage::__('tienda_categorias','Categorias')?></a>

      						  <div class="list-group lista__group2 lista__acordion">
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  </div>

      						<a href="#!" class="list-group-item list-group-item-action lista__link"><i class="fas fa-circle"></i> <?=InPage::__('tienda_categorias','Categorias')?></a>

      						  <div class="list-group lista__group2 lista__acordion">
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  </div>

      						<a href="#!" class="list-group-item list-group-item-action lista__link"><i class="fas fa-circle"></i> <?=InPage::__('tienda_categorias','Categorias')?></a>

      						  <div class="list-group lista__group2 lista__acordion">
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  </div>

      						<a href="#!" class="list-group-item list-group-item-action lista__link"><i class="fas fa-circle"></i> <?=InPage::__('tienda_categorias','Categorias')?></a>

      						  <div class="list-group lista__group2 lista__acordion">
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  </div>

      						<a href="#!" class="list-group-item list-group-item-action lista__link"><i class="fas fa-circle"></i> <?=InPage::__('tienda_categorias','Categorias')?></a>

      						  <div class="list-group lista__group2 lista__acordion">
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  </div>

      						<a href="#!" class="list-group-item list-group-item-action lista__link"><i class="fas fa-circle"></i> <?=InPage::__('tienda_categorias','Categorias')?></a>

      						  <div class="list-group lista__group2 lista__acordion">
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  </div>
      						  <a href="#!" class="list-group-item list-group-item-action lista__link"><i class="fas fa-circle"></i> <?=InPage::__('tienda_categorias','Categorias')?></a>

      						  <div class="list-group lista__group2 lista__acordion">
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  </div>

      						<a href="#!" class="list-group-item list-group-item-action lista__link"><i class="fas fa-circle"></i> <?=InPage::__('tienda_categorias','Categorias')?></a>

      						  <div class="list-group lista__group2 lista__acordion">
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  </div>

      						<a href="#!" class="list-group-item list-group-item-action lista__link"><i class="fas fa-circle"></i> <?=InPage::__('tienda_categorias','Categorias')?></a>

      						  <div class="list-group lista__group2 lista__acordion">
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  </div>

      						<a href="#!" class="list-group-item list-group-item-action lista__link"><i class="fas fa-circle"></i> <?=InPage::__('tienda_categorias','Categorias')?></a>

      						  <div class="list-group lista__group2 lista__acordion">
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  </div>

      						  <a href="#!" class="list-group-item list-group-item-action lista__link"><i class="fas fa-circle"></i> <?=InPage::__('tienda_categorias','Categorias')?></a>

      						  <div class="list-group lista__group2 lista__acordion">
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  </div>

      					</div>
      	</div>
      	<div class="container__lidera lista__container">

      		<div class="d-flex flex-wrap">

      			<div class="lista__col_marca d-flex flex-column lista__show-marca lista__bg_categorias">
      				<h5 class="lista__font"><?=InPage::__('tienda_title','Compra por marca')?></h5>
      				<a href="#" class="lista__btn" data-toggle="modal" data-target="#select_marca"><i class="fab fa-the-red-yeti lista__yeti"></i><br>
      				<?=InPage::__('tienda_btn_menu_title','Elige una marca')?></a>
      				<h5 class="lista__font mt-5"><?=InPage::__('tienda_menu_subtitle','Comprar por categoria')?></h5>
      					<div class="list-group">

      					  	<a href="#!" class="list-group-item list-group-item-action lista__link"><i class="fas fa-circle"></i> <?=InPage::__('tienda_categorias','Categorias')?></a>

      						  <div class="list-group lista__group2 lista__acordion">
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  </div>

      						<a href="#!" class="list-group-item list-group-item-action lista__link"><i class="fas fa-circle"></i> <?=InPage::__('tienda_categorias','Categorias')?></a>

      						  <div class="list-group lista__group2 lista__acordion">
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  </div>

      						<a href="#!" class="list-group-item list-group-item-action lista__link"><i class="fas fa-circle"></i> <?=InPage::__('tienda_categorias','Categorias')?></a>

      						  <div class="list-group lista__group2 lista__acordion">
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  </div>

      						<a href="#!" class="list-group-item list-group-item-action lista__link"><i class="fas fa-circle"></i> <?=InPage::__('tienda_categorias','Categorias')?></a>

      						  <div class="list-group lista__group2 lista__acordion">
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  </div>

      						<a href="#!" class="list-group-item list-group-item-action lista__link"><i class="fas fa-circle"></i> <?=InPage::__('tienda_categorias','Categorias')?></a>

      						  <div class="list-group lista__group2 lista__acordion">
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  </div>

      						<a href="#!" class="list-group-item list-group-item-action lista__link"><i class="fas fa-circle"></i> <?=InPage::__('tienda_categorias','Categorias')?></a>

      						  <div class="list-group lista__group2 lista__acordion">
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  </div>

      						<a href="#!" class="list-group-item list-group-item-action lista__link"><i class="fas fa-circle"></i> <?=InPage::__('tienda_categorias','Categorias')?></a>

      						  <div class="list-group lista__group2 lista__acordion">
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  </div>

      						<a href="#!" class="list-group-item list-group-item-action lista__link"><i class="fas fa-circle"></i> <?=InPage::__('tienda_categorias','Categorias')?></a>

      						  <div class="list-group lista__group2 lista__acordion">
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  </div>
      						  <a href="#!" class="list-group-item list-group-item-action lista__link"><i class="fas fa-circle"></i> <?=InPage::__('tienda_categorias','Categorias')?></a>

      						  <div class="list-group lista__group2 lista__acordion">
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  </div>

      						<a href="#!" class="list-group-item list-group-item-action lista__link"><i class="fas fa-circle"></i> <?=InPage::__('tienda_categorias','Categorias')?></a>

      						  <div class="list-group lista__group2 lista__acordion">
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  </div>

      						<a href="#!" class="list-group-item list-group-item-action lista__link"><i class="fas fa-circle"></i> <?=InPage::__('tienda_categorias','Categorias')?></a>

      						  <div class="list-group lista__group2 lista__acordion">
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  </div>

      						<a href="#!" class="list-group-item list-group-item-action lista__link"><i class="fas fa-circle"></i> <?=InPage::__('tienda_categorias','Categorias')?></a>

      						  <div class="list-group lista__group2 lista__acordion">
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  </div>

      						  <a href="#!" class="list-group-item list-group-item-action lista__link"><i class="fas fa-circle"></i> <?=InPage::__('tienda_categorias','Categorias')?></a>

      						  <div class="list-group lista__group2 lista__acordion">
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  	<a href="#!" class="list-group-item list-group-item-action lista__link2"><i class="far fa-circle"></i>  <?=InPage::__('tienda_subcategorias','Sub-categorias')?></a>
      						  </div>

      					</div>
      			</div>
      			<div class="lista__col_carrusel">
      				<div class="d-flex align-self-end lista__box-filtrar lista__show-filtrar">
      					<a href="#f" class="ml-auto lista__btn-filtrar" id="filtrar"><i class="fas fa-align-left"></i> <?=InPage::__('tienda_btn_filtrar','FILTRAR')?></a>
      				</div>

      				<div class="d-flex justify-content-between title__box ">
      					<h4 class="title__title"><?=InPage::__('tienda_title_carrusel','Categoria #1')?></h4>
      					<a href="#"  class="title__ver"><?=InPage::__('tienda_btn_carrusel_1','Ver todos')?></a>
      				</div>
      				<div id="carousel-example-multi" class="carousel slide carousel-multi-item v-2" data-ride="carousel">

      					<!--Controls-->
      					<a class="carousel-control-prev" href="#carusel-2" role="button" data-slide="prev" id="">
      						<span class="carousel-control-prev-icon" aria-hidden="true"></span>
      						<span class="sr-only">Previous</span>
      					</a>
      					<a class="carousel-control-next" href="#carusel-2" id="" role="button" data-slide="next">
      						<span class="carousel-control-next-icon" aria-hidden="true"></span>
      						<span class="sr-only">Next</span>
      					</a>

      					<div class="carousel-inner" role="listbox">
      						<div class="carousel-item active d-flex flex-nowrap">
      							<div class="col-lg-4 col-md-4 col-sm-6 col-12 carrusel__padding_lista">
      								<div class="carrusel__box_lista d-flex flex-column" data-toggle="modal" data-target="#producto">
      									<div class="carrusel__box-img_lista mx-auto">
      										<img src="<?= InPage::imgMod('imagen_del_producto_2','ImgP2.png'); ?>" class="carrusel__img" alt="">
      									</div>
      									<h6 class="text-center carrusel__title_lista"><?=InPage::__('tienda_title_carrusel_producto','Maizina 900grs')?></h6>
      									<span class="text-center carrusel__subtitle_lista"><?=InPage::__('tienda_precio_carrusel_producto','Bs. 250.000.00')?></span>
      									<a href="#agregando" class="carrusel__btn_agregar" id="mostrar"><i class="fa fa-shopping-cart" aria-hidden="true"></i> <?=InPage::__('tienda_btn_carrusel_producto','Agregar')?></a>
      									<div  class="d-flex flex-nowrap mt-auto carrusel__btn-show">
      										<a href="#salir" class="carrusel__btn_salir"><i class="fa fa-times" aria-hidden="true"></i></a>
      										<a href="#restar" class="carrusel__btn_restar carrusel__btn_otro"><i class="fa fa-minus" aria-hidden="true"></i></a>
      										<span href="#valor" class="carrusel__numero carrusel__btn_otro">1</span>
      										<a href="#sumar" class="carrusel__btn_sumar carrusel__btn_otro"><i class="fa fa-plus" aria-hidden="true"></i></a>
      									</div>
      								</div>
      							</div>
      							<div class="col-lg-4 col-md-4 col-sm-6 col-12 carrusel__padding_lista">
      								<div class="carrusel__box_lista d-flex flex-column">
      									<div class="carrusel__box-img_lista mx-auto" data-toggle="modal" data-target="#producto">
      										<img src="<?= InPage::imgMod('imagen_del_producto_2','ImgP2.png'); ?>" class="carrusel__img" alt="">
      									</div>
      									<h6 class="text-center carrusel__title_lista"><?=InPage::__('tienda_title_carrusel_producto','Maizina 900grs')?></h6>
      									<span class="text-center carrusel__subtitle_lista"><?=InPage::__('tienda_precio_carrusel_producto','Bs. 250.000.00')?></span>
      									<a href="#agregando" class="carrusel__btn_agregar" id="mostrar"><i class="fa fa-shopping-cart" aria-hidden="true"></i> <?=InPage::__('tienda_btn_carrusel_producto','Agregar')?></a>
      									<div  class="d-flex flex-nowrap mt-auto carrusel__btn-show">
      										<a href="#salir" class="carrusel__btn_salir"><i class="fa fa-times" aria-hidden="true"></i></a>
      										<a href="#restar" class="carrusel__btn_restar carrusel__btn_otro"><i class="fa fa-minus" aria-hidden="true"></i></a>
      										<span href="#valor" class="carrusel__numero carrusel__btn_otro">1</span>
      										<a href="#sumar" class="carrusel__btn_sumar carrusel__btn_otro"><i class="fa fa-plus" aria-hidden="true"></i></a>
      									</div>
      								</div>
      							</div>
      							<div class="col-lg-4 col-md-4 col-sm-6 col-12 carrusel__padding_lista">
      								<div class="carrusel__box_lista d-flex flex-column">
      									<div class="carrusel__box-img_lista mx-auto" data-toggle="modal" data-target="#producto">
      										<img src="<?= InPage::imgMod('imagen_del_producto_2','ImgP2.png'); ?>" class="carrusel__img" alt="">
      									</div>
      									<h6 class="text-center carrusel__title_lista"><?=InPage::__('tienda_title_carrusel_producto','Maizina 900grs')?></h6>
      									<span class="text-center carrusel__subtitle_lista"><?=InPage::__('tienda_precio_carrusel_producto','Bs. 250.000.00')?></span>
      									<a href="#agregando" class="carrusel__btn_agregar" id="mostrar"><i class="fa fa-shopping-cart" aria-hidden="true"></i> <?=InPage::__('tienda_btn_carrusel_producto','Agregar')?></a>
      									<div  class="d-flex flex-nowrap mt-auto carrusel__btn-show">
      										<a href="#salir" class="carrusel__btn_salir"><i class="fa fa-times" aria-hidden="true"></i></a>
      										<a href="#restar" class="carrusel__btn_restar carrusel__btn_otro"><i class="fa fa-minus" aria-hidden="true"></i></a>
      										<span href="#valor" class="carrusel__numero carrusel__btn_otro">1</span>
      										<a href="#sumar" class="carrusel__btn_sumar carrusel__btn_otro"><i class="fa fa-plus" aria-hidden="true"></i></a>
      									</div>
      								</div>
      							</div>
      							<div class="col-lg-4 col-md-4 col-sm-6 col-12 carrusel__padding_lista">
      								<div class="carrusel__box_lista d-flex flex-column">
      									<div class="carrusel__box-img_lista mx-auto" data-toggle="modal" data-target="#producto">
      										<img src="<?= InPage::imgMod('imagen_del_producto_2','ImgP2.png'); ?>" class="carrusel__img" alt="">
      									</div>
      									<h6 class="text-center carrusel__title_lista"><?=InPage::__('tienda_title_carrusel_producto','Maizina 900grs')?></h6>
      									<span class="text-center carrusel__subtitle_lista"><?=InPage::__('tienda_precio_carrusel_producto','Bs. 250.000.00')?></span>
      									<a href="#agregando" class="carrusel__btn_agregar" id="mostrar"><i class="fa fa-shopping-cart" aria-hidden="true"></i> <?=InPage::__('tienda_btn_carrusel_producto','Agregar')?></a>
      									<div  class="d-flex flex-nowrap mt-auto carrusel__btn-show">
      										<a href="#salir" class="carrusel__btn_salir"><i class="fa fa-times" aria-hidden="true"></i></a>
      										<a href="#restar" class="carrusel__btn_restar carrusel__btn_otro"><i class="fa fa-minus" aria-hidden="true"></i></a>
      										<span href="#valor" class="carrusel__numero carrusel__btn_otro">1</span>
      										<a href="#sumar" class="carrusel__btn_sumar carrusel__btn_otro"><i class="fa fa-plus" aria-hidden="true"></i></a>
      									</div>
      								</div>
      							</div>
      							<div class="col-lg-4 col-md-4 col-sm-6 col-12 carrusel__padding_lista">
      								<div class="carrusel__box_lista d-flex flex-column">
      									<div class="carrusel__box-img_lista mx-auto" data-toggle="modal" data-target="#producto">
      										<img src="<?= InPage::imgMod('imagen_del_producto_2','ImgP2.png'); ?>" class="carrusel__img" alt="">
      									</div>
      									<h6 class="text-center carrusel__title_lista"><?=InPage::__('tienda_title_carrusel_producto','Maizina 900grs')?></h6>
      									<span class="text-center carrusel__subtitle_lista"><?=InPage::__('tienda_precio_carrusel_producto','Bs. 250.000.00')?></span>
      									<a href="#agregando" class="carrusel__btn_agregar" id="mostrar"><i class="fa fa-shopping-cart" aria-hidden="true"></i> <?=InPage::__('tienda_btn_carrusel_producto','Agregar')?></a>
      									<div  class="d-flex flex-nowrap mt-auto carrusel__btn-show">
      										<a href="#salir" class="carrusel__btn_salir"><i class="fa fa-times" aria-hidden="true"></i></a>
      										<a href="#restar" class="carrusel__btn_restar carrusel__btn_otro"><i class="fa fa-minus" aria-hidden="true"></i></a>
      										<span href="#valor" class="carrusel__numero carrusel__btn_otro">1</span>
      										<a href="#sumar" class="carrusel__btn_sumar carrusel__btn_otro"><i class="fa fa-plus" aria-hidden="true"></i></a>
      									</div>
      								</div>
      							</div>
      						</div>
      					</div>

      				</div>
      				<div class="d-flex justify-content-between title__box mt-lg-5 mt-3">
      					<h4 class="title__title"><?=InPage::__('tienda_title_carrusel','Categoria #1')?></h4>
      					<a href="#"  class="title__ver"><?=InPage::__('tienda_btn_carrusel_1','Ver todos')?></a>
      				</div>
      				<div id="carousel-example-multi" class="carousel slide carousel-multi-item v-2" data-ride="carousel">

      					<!--Controls-->
      					<a class="carousel-control-prev" href="#carusel-2" role="button" data-slide="prev" id="">
      						<span class="carousel-control-prev-icon" aria-hidden="true"></span>
      						<span class="sr-only">Previous</span>
      					</a>
      					<a class="carousel-control-next" href="#carusel-2" id="" role="button" data-slide="next">
      						<span class="carousel-control-next-icon" aria-hidden="true"></span>
      						<span class="sr-only">Next</span>
      					</a>

      					<div class="carousel-inner" role="listbox">
      						<div class="carousel-item active d-flex flex-nowrap">
      							<div class="col-lg-4 col-md-4 col-sm-6 col-12 carrusel__padding_lista">
      								<div class="carrusel__box_lista d-flex flex-column" data-toggle="modal" data-target="#producto">
      									<div class="carrusel__box-img_lista mx-auto">
      										<img src="<?= InPage::imgMod('imagen_del_producto_2','ImgP2.png'); ?>" class="carrusel__img" alt="">
      									</div>
      									<h6 class="text-center carrusel__title_lista"><?=InPage::__('tienda_title_carrusel_producto','Maizina 900grs')?></h6>
      									<span class="text-center carrusel__subtitle_lista"><?=InPage::__('tienda_precio_carrusel_producto','Bs. 250.000.00')?></span>
      									<a href="#agregando" class="carrusel__btn_agregar" id="mostrar"><i class="fa fa-shopping-cart" aria-hidden="true"></i> <?=InPage::__('tienda_btn_carrusel_producto','Agregar')?></a>
      									<div  class="d-flex flex-nowrap mt-auto carrusel__btn-show">
      										<a href="#salir" class="carrusel__btn_salir"><i class="fa fa-times" aria-hidden="true"></i></a>
      										<a href="#restar" class="carrusel__btn_restar carrusel__btn_otro"><i class="fa fa-minus" aria-hidden="true"></i></a>
      										<span href="#valor" class="carrusel__numero carrusel__btn_otro">1</span>
      										<a href="#sumar" class="carrusel__btn_sumar carrusel__btn_otro"><i class="fa fa-plus" aria-hidden="true"></i></a>
      									</div>
      								</div>
      							</div>
      							<div class="col-lg-4 col-md-4 col-sm-6 col-12 carrusel__padding_lista">
      								<div class="carrusel__box_lista d-flex flex-column">
      									<div class="carrusel__box-img_lista mx-auto" data-toggle="modal" data-target="#producto">
      										<img src="<?= InPage::imgMod('imagen_del_producto_2','ImgP2.png'); ?>" class="carrusel__img" alt="">
      									</div>
      									<h6 class="text-center carrusel__title_lista"><?=InPage::__('tienda_title_carrusel_producto','Maizina 900grs')?></h6>
      									<span class="text-center carrusel__subtitle_lista"><?=InPage::__('tienda_precio_carrusel_producto','Bs. 250.000.00')?></span>
      									<a href="#agregando" class="carrusel__btn_agregar" id="mostrar"><i class="fa fa-shopping-cart" aria-hidden="true"></i> <?=InPage::__('tienda_btn_carrusel_producto','Agregar')?></a>
      									<div  class="d-flex flex-nowrap mt-auto carrusel__btn-show">
      										<a href="#salir" class="carrusel__btn_salir"><i class="fa fa-times" aria-hidden="true"></i></a>
      										<a href="#restar" class="carrusel__btn_restar carrusel__btn_otro"><i class="fa fa-minus" aria-hidden="true"></i></a>
      										<span href="#valor" class="carrusel__numero carrusel__btn_otro">1</span>
      										<a href="#sumar" class="carrusel__btn_sumar carrusel__btn_otro"><i class="fa fa-plus" aria-hidden="true"></i></a>
      									</div>
      								</div>
      							</div>
      							<div class="col-lg-4 col-md-4 col-sm-6 col-12 carrusel__padding_lista">
      								<div class="carrusel__box_lista d-flex flex-column">
      									<div class="carrusel__box-img_lista mx-auto" data-toggle="modal" data-target="#producto">
      										<img src="<?= InPage::imgMod('imagen_del_producto_2','ImgP2.png'); ?>" class="carrusel__img" alt="">
      									</div>
      									<h6 class="text-center carrusel__title_lista"><?=InPage::__('tienda_title_carrusel_producto','Maizina 900grs')?></h6>
      									<span class="text-center carrusel__subtitle_lista"><?=InPage::__('tienda_precio_carrusel_producto','Bs. 250.000.00')?></span>
      									<a href="#agregando" class="carrusel__btn_agregar" id="mostrar"><i class="fa fa-shopping-cart" aria-hidden="true"></i> <?=InPage::__('tienda_btn_carrusel_producto','Agregar')?></a>
      									<div  class="d-flex flex-nowrap mt-auto carrusel__btn-show">
      										<a href="#salir" class="carrusel__btn_salir"><i class="fa fa-times" aria-hidden="true"></i></a>
      										<a href="#restar" class="carrusel__btn_restar carrusel__btn_otro"><i class="fa fa-minus" aria-hidden="true"></i></a>
      										<span href="#valor" class="carrusel__numero carrusel__btn_otro">1</span>
      										<a href="#sumar" class="carrusel__btn_sumar carrusel__btn_otro"><i class="fa fa-plus" aria-hidden="true"></i></a>
      									</div>
      								</div>
      							</div>
      							<div class="col-lg-4 col-md-4 col-sm-6 col-12 carrusel__padding_lista">
      								<div class="carrusel__box_lista d-flex flex-column">
      									<div class="carrusel__box-img_lista mx-auto" data-toggle="modal" data-target="#producto">
      										<img src="<?= InPage::imgMod('imagen_del_producto_2','ImgP2.png'); ?>" class="carrusel__img" alt="">
      									</div>
      									<h6 class="text-center carrusel__title_lista"><?=InPage::__('tienda_title_carrusel_producto','Maizina 900grs')?></h6>
      									<span class="text-center carrusel__subtitle_lista"><?=InPage::__('tienda_precio_carrusel_producto','Bs. 250.000.00')?></span>
      									<a href="#agregando" class="carrusel__btn_agregar" id="mostrar"><i class="fa fa-shopping-cart" aria-hidden="true"></i> <?=InPage::__('tienda_btn_carrusel_producto','Agregar')?></a>
      									<div  class="d-flex flex-nowrap mt-auto carrusel__btn-show">
      										<a href="#salir" class="carrusel__btn_salir"><i class="fa fa-times" aria-hidden="true"></i></a>
      										<a href="#restar" class="carrusel__btn_restar carrusel__btn_otro"><i class="fa fa-minus" aria-hidden="true"></i></a>
      										<span href="#valor" class="carrusel__numero carrusel__btn_otro">1</span>
      										<a href="#sumar" class="carrusel__btn_sumar carrusel__btn_otro"><i class="fa fa-plus" aria-hidden="true"></i></a>
      									</div>
      								</div>
      							</div>
      							<div class="col-lg-4 col-md-4 col-sm-6 col-12 carrusel__padding_lista">
      								<div class="carrusel__box_lista d-flex flex-column">
      									<div class="carrusel__box-img_lista mx-auto" data-toggle="modal" data-target="#producto">
      										<img src="<?= InPage::imgMod('imagen_del_producto_2','ImgP2.png'); ?>" class="carrusel__img" alt="">
      									</div>
      									<h6 class="text-center carrusel__title_lista"><?=InPage::__('tienda_title_carrusel_producto','Maizina 900grs')?></h6>
      									<span class="text-center carrusel__subtitle_lista"><?=InPage::__('tienda_precio_carrusel_producto','Bs. 250.000.00')?></span>
      									<a href="#agregando" class="carrusel__btn_agregar" id="mostrar"><i class="fa fa-shopping-cart" aria-hidden="true"></i> <?=InPage::__('tienda_btn_carrusel_producto','Agregar')?></a>
      									<div  class="d-flex flex-nowrap mt-auto carrusel__btn-show">
      										<a href="#salir" class="carrusel__btn_salir"><i class="fa fa-times" aria-hidden="true"></i></a>
      										<a href="#restar" class="carrusel__btn_restar carrusel__btn_otro"><i class="fa fa-minus" aria-hidden="true"></i></a>
      										<span href="#valor" class="carrusel__numero carrusel__btn_otro">1</span>
      										<a href="#sumar" class="carrusel__btn_sumar carrusel__btn_otro"><i class="fa fa-plus" aria-hidden="true"></i></a>
      									</div>
      								</div>
      							</div>
      						</div>
      					</div>

      				</div>
      				<div class="d-flex justify-content-between title__box mt-lg-5 mt-3">
      					<h4 class="title__title"><?=InPage::__('tienda_title_carrusel','Categoria #1')?></h4>
      					<a href="#"  class="title__ver"><?=InPage::__('tienda_btn_carrusel_1','Ver todos')?></a>
      				</div>
      				<div id="carousel-example-multi" class="carousel slide carousel-multi-item v-2" data-ride="carousel">

      					<!--Controls-->
      					<a class="carousel-control-prev" href="#carusel-2" role="button" data-slide="prev" id="">
      						<span class="carousel-control-prev-icon" aria-hidden="true"></span>
      						<span class="sr-only">Previous</span>
      					</a>
      					<a class="carousel-control-next" href="#carusel-2" id="" role="button" data-slide="next">
      						<span class="carousel-control-next-icon" aria-hidden="true"></span>
      						<span class="sr-only">Next</span>
      					</a>

      					<div class="carousel-inner" role="listbox">
      						<div class="carousel-item active d-flex flex-nowrap">
      							<div class="col-lg-4 col-md-4 col-sm-6 col-12 carrusel__padding_lista">
      								<div class="carrusel__box_lista d-flex flex-column" data-toggle="modal" data-target="#producto">
      									<div class="carrusel__box-img_lista mx-auto">
      										<img src="<?= InPage::imgMod('imagen_del_producto_2','ImgP2.png'); ?>" class="carrusel__img" alt="">
      									</div>
      									<h6 class="text-center carrusel__title_lista"><?=InPage::__('tienda_title_carrusel_producto','Maizina 900grs')?></h6>
      									<span class="text-center carrusel__subtitle_lista"><?=InPage::__('tienda_precio_carrusel_producto','Bs. 250.000.00')?></span>
      									<a href="#agregando" class="carrusel__btn_agregar" id="mostrar"><i class="fa fa-shopping-cart" aria-hidden="true"></i> <?=InPage::__('tienda_btn_carrusel_producto','Agregar')?></a>
      									<div  class="d-flex flex-nowrap mt-auto carrusel__btn-show">
      										<a href="#salir" class="carrusel__btn_salir"><i class="fa fa-times" aria-hidden="true"></i></a>
      										<a href="#restar" class="carrusel__btn_restar carrusel__btn_otro"><i class="fa fa-minus" aria-hidden="true"></i></a>
      										<span href="#valor" class="carrusel__numero carrusel__btn_otro">1</span>
      										<a href="#sumar" class="carrusel__btn_sumar carrusel__btn_otro"><i class="fa fa-plus" aria-hidden="true"></i></a>
      									</div>
      								</div>
      							</div>
      							<div class="col-lg-4 col-md-4 col-sm-6 col-12 carrusel__padding_lista">
      								<div class="carrusel__box_lista d-flex flex-column">
      									<div class="carrusel__box-img_lista mx-auto" data-toggle="modal" data-target="#producto">
      										<img src="<?= InPage::imgMod('imagen_del_producto_2','ImgP2.png'); ?>" class="carrusel__img" alt="">
      									</div>
      									<h6 class="text-center carrusel__title_lista"><?=InPage::__('tienda_title_carrusel_producto','Maizina 900grs')?></h6>
      									<span class="text-center carrusel__subtitle_lista"><?=InPage::__('tienda_precio_carrusel_producto','Bs. 250.000.00')?></span>
      									<a href="#agregando" class="carrusel__btn_agregar" id="mostrar"><i class="fa fa-shopping-cart" aria-hidden="true"></i> <?=InPage::__('tienda_btn_carrusel_producto','Agregar')?></a>
      									<div  class="d-flex flex-nowrap mt-auto carrusel__btn-show">
      										<a href="#salir" class="carrusel__btn_salir"><i class="fa fa-times" aria-hidden="true"></i></a>
      										<a href="#restar" class="carrusel__btn_restar carrusel__btn_otro"><i class="fa fa-minus" aria-hidden="true"></i></a>
      										<span href="#valor" class="carrusel__numero carrusel__btn_otro">1</span>
      										<a href="#sumar" class="carrusel__btn_sumar carrusel__btn_otro"><i class="fa fa-plus" aria-hidden="true"></i></a>
      									</div>
      								</div>
      							</div>
      							<div class="col-lg-4 col-md-4 col-sm-6 col-12 carrusel__padding_lista">
      								<div class="carrusel__box_lista d-flex flex-column">
      									<div class="carrusel__box-img_lista mx-auto" data-toggle="modal" data-target="#producto">
      										<img src="<?= InPage::imgMod('imagen_del_producto_2','ImgP2.png'); ?>" class="carrusel__img" alt="">
      									</div>
      									<h6 class="text-center carrusel__title_lista"><?=InPage::__('tienda_title_carrusel_producto','Maizina 900grs')?></h6>
      									<span class="text-center carrusel__subtitle_lista"><?=InPage::__('tienda_precio_carrusel_producto','Bs. 250.000.00')?></span>
      									<a href="#agregando" class="carrusel__btn_agregar" id="mostrar"><i class="fa fa-shopping-cart" aria-hidden="true"></i> <?=InPage::__('tienda_btn_carrusel_producto','Agregar')?></a>
      									<div  class="d-flex flex-nowrap mt-auto carrusel__btn-show">
      										<a href="#salir" class="carrusel__btn_salir"><i class="fa fa-times" aria-hidden="true"></i></a>
      										<a href="#restar" class="carrusel__btn_restar carrusel__btn_otro"><i class="fa fa-minus" aria-hidden="true"></i></a>
      										<span href="#valor" class="carrusel__numero carrusel__btn_otro">1</span>
      										<a href="#sumar" class="carrusel__btn_sumar carrusel__btn_otro"><i class="fa fa-plus" aria-hidden="true"></i></a>
      									</div>
      								</div>
      							</div>
      							<div class="col-lg-4 col-md-4 col-sm-6 col-12 carrusel__padding_lista">
      								<div class="carrusel__box_lista d-flex flex-column">
      									<div class="carrusel__box-img_lista mx-auto" data-toggle="modal" data-target="#producto">
      										<img src="<?= InPage::imgMod('imagen_del_producto_2','ImgP2.png'); ?>" class="carrusel__img" alt="">
      									</div>
      									<h6 class="text-center carrusel__title_lista"><?=InPage::__('tienda_title_carrusel_producto','Maizina 900grs')?></h6>
      									<span class="text-center carrusel__subtitle_lista"><?=InPage::__('tienda_precio_carrusel_producto','Bs. 250.000.00')?></span>
      									<a href="#agregando" class="carrusel__btn_agregar" id="mostrar"><i class="fa fa-shopping-cart" aria-hidden="true"></i> <?=InPage::__('tienda_btn_carrusel_producto','Agregar')?></a>
      									<div  class="d-flex flex-nowrap mt-auto carrusel__btn-show">
      										<a href="#salir" class="carrusel__btn_salir"><i class="fa fa-times" aria-hidden="true"></i></a>
      										<a href="#restar" class="carrusel__btn_restar carrusel__btn_otro"><i class="fa fa-minus" aria-hidden="true"></i></a>
      										<span href="#valor" class="carrusel__numero carrusel__btn_otro">1</span>
      										<a href="#sumar" class="carrusel__btn_sumar carrusel__btn_otro"><i class="fa fa-plus" aria-hidden="true"></i></a>
      									</div>
      								</div>
      							</div>
      							<div class="col-lg-4 col-md-4 col-sm-6 col-12 carrusel__padding_lista">
      								<div class="carrusel__box_lista d-flex flex-column">
      									<div class="carrusel__box-img_lista mx-auto" data-toggle="modal" data-target="#producto">
      										<img src="<?= InPage::imgMod('imagen_del_producto_2','ImgP2.png'); ?>" class="carrusel__img" alt="">
      									</div>
      									<h6 class="text-center carrusel__title_lista"><?=InPage::__('tienda_title_carrusel_producto','Maizina 900grs')?></h6>
      									<span class="text-center carrusel__subtitle_lista"><?=InPage::__('tienda_precio_carrusel_producto','Bs. 250.000.00')?></span>
      									<a href="#agregando" class="carrusel__btn_agregar" id="mostrar"><i class="fa fa-shopping-cart" aria-hidden="true"></i> <?=InPage::__('tienda_btn_carrusel_producto','Agregar')?></a>
      									<div  class="d-flex flex-nowrap mt-auto carrusel__btn-show">
      										<a href="#salir" class="carrusel__btn_salir"><i class="fa fa-times" aria-hidden="true"></i></a>
      										<a href="#restar" class="carrusel__btn_restar carrusel__btn_otro"><i class="fa fa-minus" aria-hidden="true"></i></a>
      										<span href="#valor" class="carrusel__numero carrusel__btn_otro">1</span>
      										<a href="#sumar" class="carrusel__btn_sumar carrusel__btn_otro"><i class="fa fa-plus" aria-hidden="true"></i></a>
      									</div>
      								</div>
      							</div>
      						</div>
      					</div>

      				</div>
      			</div>

      		</div>
      	</div>
      </div>
      <!-- Central Modal Small Select -->
      <div class="modal fade select__fondo" id="select_marca" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">

        <!-- Change class .modal-sm to change the size of the modal -->
        <div class="modal-dialog modal-lg" role="document">


          <div class="modal-content select__modal_body">

            <div class="modal-body ">
              <div class="d-flex flex-nowrap select__box-title">
                <h4 class="select__title mb-0 my-auto"><?=InPage::__('tienda_title_modal','Elige una marca para chequear los productos')?></h4>
                <a href="#saquen_me_de_aqui" class="ml-auto select__btn" data-dismiss="modal" aria-label="Close"><i class="fas fa-times"></i></a>
              </div>
              <div class="select__box">
                  <div class="d-flex flex-wrap justify-content-center">
                      <a href="#marca" class="select__imgs-box text-center"><img src="static/<?= InPage::imgMod('imagen_marca_del_producto','Maizina-Americana.png'); ?>" class="select__imgs" alt="">
                      <?=InPage::__('tienda_subtitle_modal','Maizina Americana')?></a>
                      <a href="#marca" class="select__imgs-box text-center"><img src="static/<?= InPage::imgMod('imagen_marca_del_producto','Maizina-Americana.png'); ?>" class="select__imgs" alt="">
                      <?=InPage::__('tienda_subtitle_modal','Maizina Americana')?></a>
                      <a href="#marca" class="select__imgs-box text-center"><img src="static/<?= InPage::imgMod('imagen_marca_del_producto','Maizina-Americana.png'); ?>" class="select__imgs" alt="">
                      <?=InPage::__('tienda_subtitle_modal','Maizina Americana')?></a>
                      <a href="#marca" class="select__imgs-box text-center"><img src="static/<?= InPage::imgMod('imagen_marca_del_producto','Maizina-Americana.png'); ?>" class="select__imgs" alt="">
                      <?=InPage::__('tienda_subtitle_modal','Maizina Americana')?></a>
                      <a href="#marca" class="select__imgs-box text-center"><img src="static/<?= InPage::imgMod('imagen_marca_del_producto','Maizina-Americana.png'); ?>" class="select__imgs" alt="">
                      <?=InPage::__('tienda_subtitle_modal','Maizina Americana')?></a>
                      <a href="#marca" class="select__imgs-box text-center"><img src="static/<?= InPage::imgMod('imagen_marca_del_producto','Maizina-Americana.png'); ?>" class="select__imgs" alt="">
                      <?=InPage::__('tienda_subtitle_modal','Maizina Americana')?></a>
                      <a href="#marca" class="select__imgs-box text-center"><img src="static/<?= InPage::imgMod('imagen_marca_del_producto','Maizina-Americana.png'); ?>" class="select__imgs" alt="">
                      <?=InPage::__('tienda_subtitle_modal','Maizina Americana')?></a>
                      <a href="#marca" class="select__imgs-box text-center"><img src="static/<?= InPage::imgMod('imagen_marca_del_producto','Maizina-Americana.png'); ?>" class="select__imgs" alt="">
                      <?=InPage::__('tienda_subtitle_modal','Maizina Americana')?></a>
                      <a href="#marca" class="select__imgs-box text-center"><img src="static/<?= InPage::imgMod('imagen_marca_del_producto','Maizina-Americana.png'); ?>" class="select__imgs" alt="">
                      <?=InPage::__('tienda_subtitle_modal','Maizina Americana')?></a>
                      <a href="#marca" class="select__imgs-box text-center"><img src="static/<?= InPage::imgMod('imagen_marca_del_producto','Maizina-Americana.png'); ?>" class="select__imgs" alt="">
                      <?=InPage::__('tienda_subtitle_modal','Maizina Americana')?></a>
                      <a href="#marca" class="select__imgs-box text-center"><img src="static/<?= InPage::imgMod('imagen_marca_del_producto','Maizina-Americana.png'); ?>" class="select__imgs" alt="">
                      <?=InPage::__('tienda_subtitle_modal','Maizina Americana')?></a>
                      <a href="#marca" class="select__imgs-box text-center"><img src="static/<?= InPage::imgMod('imagen_marca_del_producto','Maizina-Americana.png'); ?>" class="select__imgs" alt="">
                      <?=InPage::__('tienda_subtitle_modal','Maizina Americana')?></a>
                  </div>
              </div>

            </div>

          </div>
        </div>
      </div>
      <!-- Central Modal Small Select -->
       <!-- Central Modal Small -->
          <div class="modal fade productos__bg" id="producto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                aria-hidden="true">

                <!-- Change class .modal-sm to change the size of the modal -->
              <div class="modal-dialog modal-dialog-centered container__lidera productos__container" role="document">


                  <div class="modal-content productos__modal_body">
                    <div class="modal-body">
                      <div class="d-flex justify-content-end">
                        <a href="#" class="ml-auto productos__btn_salir" data-dismiss="modal" aria-label="Close"><i class="fas fa-times"></i></a>
                      </div>
                      <div class="d-flex flex-wrap">
                        <div class="col-12 col-md-12 col-lg-6 productos__padding">
                          <div href="#produc" class="productos__box_imgs">
                            <img src="static/<?= InPage::imgMod('imagen_marca_del_producto','Maizina-Americana.png'); ?>" class="productos__imgs" alt="">
                          </div>
                        </div>
                        <div class="col-12 col-md-12 col-lg-6 productos__padding">
                          <h5 class="productos__title_proct mb-1"><?=InPage::__('tienda_title_modal_2','Maizina Americana')?></h5>
                          <hr class="mb-1 mt-0">
                          <p class="productos__font productos__descrip mb-2"><?=InPage::__('tienda_descripcion_modal_2','Almidón de maíz o maicena, harina para hacer pan, pastas, bizcochos, bases de pizza.<br> Producto blanco, en polvo, muy suave y refinado, extraído de los mejores granos de maíz.')?></p>
                          <hr class="my-2">
                          <h6 class="productos__font productos__detalles"><?=InPage::__('tienda_detalles_1_modal_2','Disponible: 70 unidades')?><h6>
                          <h6 class="productos__font productos__detalles"><?=InPage::__('tienda_detalles_2_modal_2','SKU:PDD-22062020')?></h6>
                          <h6 class="productos__font productos__detalles"><?=InPage::__('tienda_detalles_3_modal_2','Another important info #1: Lorem')?></h6>
                          <h6 class="productos__font productos__detalles"><?=InPage::__('tienda_detalles_4_modal_2','Another important info #2: Lorem')?></h6>
                          <hr class="my-2">
                          <div class="d-flex justify-content-sm-between productos__movil_colum">
                            <h5 class="productos__precios my-auto"><?=InPage::__('tienda_precios_modal_2','Precio: 220,000.00 Bs')?></h5>
                            <a href="#comprado__sorry" class="productos__btn_ag regar"><i class="fa fa-shopping-cart" aria-hidden="true"></i><?=InPage::__('tienda_btn_carrusel_producto','Agregar')?></a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
      <!-- Central Modal Small -->

<?php get_footer();?>
