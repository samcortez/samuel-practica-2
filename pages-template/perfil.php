<?php /* Template Name: Pagina de perfil */ ?>
<?php /* Template description: Para jugar con elementor */ ?>
<?php use Opalo\Helpers\InPage; ?>
<?php get_header(); ?>

<div class="tab__bg">
    	<div class="container__lidera tab__container">
    		<div class="d-flex flex-wrap">
    		<div class="d-flex flex-column col-12 col-lg-4 col-md-5 tab__padding_user tab__movil">
    			<div class="d-flex flex-nowrap tab__box_perfil  px-0 mb-2 bg-white">
    				<div class="tab__box_img">
    					<img src="<?= InPage::imgMod('perfil_icono_persona','person.png'); ?>" class="tab__img_person" class="" alt="">
    				</div>
    				<div class="d-flex flex-column tab__user my-auto">
    					<h6 class="tab__font"><?=InPage::__('perfil_name','Nombre: Unnamed')?></h6>
    					<h6 class="tab__font"><?=InPage::__('perfil_rango','Rango: S')?></h6>
    				</div>
    			</div>
    				<ul class="nav nav-tabs list-group" id="myTab" role="tablist">
    				  <li class="">
    				    <a class="tab__link list-group-item list-group-item-action  active" id="home-tab" data-toggle="tab" href="#escritorio" role="tab" aria-controls="home"
    				      aria-selected="true"><i class="fas fa-circle"></i> <?=InPage::__('perfil_inputs_1','Escritorio')?></a>
    				  </li>
    				  <li class="">
    				    <a class="tab__link list-group-item list-group-item-action" id="profile-tab" data-toggle="tab" href="#pedidos" role="tab" aria-controls="profile"
    				      aria-selected="false"><i class="fas fa-circle"></i> <?=InPage::__('perfil_inputs_2','Pedidos')?></a>
    				  </li>
    				  <li class="">
    				    <a class="tab__link list-group-item list-group-item-action" id="contact-tab" data-toggle="tab" href="#direcciones" role="tab" aria-controls="contact"
    				      aria-selected="false"><i class="fas fa-circle"></i> <?=InPage::__('perfil_inputs_3','Direcciones')?></a>
    				  </li>
    				  <li class="">
    				    <a class="tab__link list-group-item list-group-item-action" id="contact-tab" data-toggle="tab" href="#ajustes" role="tab" aria-controls="contact"
    				      aria-selected="false"><i class="fas fa-circle"></i> <?=InPage::__('perfil_inputs_4','Ajustes')?></a>
    				  </li>
    				</ul>
    		</div>
    		<div class="col-12 col-lg-8 tab__padding">
    			<div class="tab-content" id="myTabContent">
    		
    			  	<div class="tab-pane fade show active" 
    			  	id="escritorio" role="tabpanel" aria-labelledby="home-tab">
    			  		<h2 class="escritorio__title"><?=InPage::__('perfil_name','Nombre: Unnamed')?></h2>
    			  		<div class="d-flex flex-wrap">
    			  			<div class="escritorio__col">
    			  				<div class="escritorio__box text-center ">
    			  					<i class="far fa-star escritorio__start"></i>
    			  					<h4 class="escritorio__subtitle mt-0"><?=InPage::__('perfil_inputs_1','Escritorio')?></h4>
    			  				</div>	
    			  			</div>
    			  			<div class="escritorio__col">
    			  				<div class="escritorio__box text-center ">
    			  					<i class="far fa-star escritorio__start"></i>
    			  					<h4 class="escritorio__subtitle mt-0"><?=InPage::__('perfil_inputs_2','Pedidos')?></h4>
    			  				</div>	
    			  			</div>
    			  			<div class="escritorio__col">
    			  				<div class="escritorio__box text-center ">
    			  					<i class="far fa-star escritorio__start"></i>
    			  					<h4 class="escritorio__subtitle mt-0"><?=InPage::__('perfil_inputs_3','Direcciones')?></h4>
    			  				</div>	
    			  			</div>
    			  			<div class="escritorio__col">
    			  				<div class="escritorio__box text-center ">
    			  					<i class="far fa-star escritorio__start"></i>
    			  					<h4 class="escritorio__subtitle mt-0"><?=InPage::__('perfil_inputs_4','Ajustes')?></h4>
    			  				</div>	
    			  			</div>
    			  			<div class="escritorio__col">
    			  				<div class="escritorio__box text-center ">
    			  					<i class="far fa-star escritorio__start"></i>
    			  					<h4 class="escritorio__subtitle mt-0"><?=InPage::__('perfil_inputs_5','Ir a la tienda')?></h4>
    			  				</div>	
    			  			</div>
    			  		</div>
    			  	</div>			
    			  	 <div class="tab-pane fade" id="pedidos" 
    			  	 role="tabpanel" aria-labelledby="profile-tab">
    			  		<h2 class="pedidos__title"><?=InPage::__('perfil_pedido_title','Pedido #3465a')?></h2>
    			  		<h6 class="pedidos__subtitle"><?=InPage::__('perfil_pedido_fecha','Se realizó el 1 julio, 2020 y está actualmente Procesando')?></h6>
    			  		<div class="pedidos__box d-flex flex-column">
    			  			<div class="d-flex flex-nowrap">
    			  				<span class="col-6 pedidos__font_orange"><?=InPage::__('perfil_pedido_subtitles_1','Producto')?></span>
    			  				<span class="col-6 pedidos__font_orange"><?=InPage::__('perfil_pedido_subtitles_2','Monto')?></span>
    			  			</div>
    			  			<hr class="my-0 pedidos__line_black">
    			  			<div class="d-flex flex-nowrap">
    			  				<span class="col-6 pedidos__font_productos"><?=InPage::__('perfil_producto_1','Harina de maiz Doña Golla x 4')?></span>
    			  				<span class="col-6 pedidos__font_productos"><?=InPage::__('perfil_producto_precio_1','Bs 7.100.500')?></span>
    			  			</div>
    			  			<hr class="my-0 pedidos__line_orange">
    			  			<div class="d-flex flex-nowrap">
    			  				<span class="col-6 pedidos__font_productos"><?=InPage::__('perfil_producto_2','Harina de trigo Doña Golla x 10')?></span>
    			  				<span class="col-6 pedidos__font_productos"><?=InPage::__('perfil_producto_precio_2','Bs 700.100.500')?></span>
    			  			</div>
    			  	
    			  			
    			  			<div class="mt-auto">
    			  				<h6 class="mb-1 text-left pedidos__font_orange"><?=InPage::__('perfil_pedido_subtitles_3','Dirección de facturación')?></h6>
    			  				<hr class=" my-0 pedidos__line_black">
    			  				<p class="pedidos__font_gris"><?=InPage::__('perfil_producto_name','Eduardo Lara')?></p>
    			  				<p class="pedidos__font_gris"><?=InPage::__('perfil_producto_direccion','Av, Libertador, 24 de Mayo')?></p>
    			  				<p class="pedidos__font_gris"><?=InPage::__('perfil_producto_codigo','J-56445454-5')?></p>
    			  				<p class="pedidos__font_gris"><?=InPage::__('perfil_producto_tlf','J+58 412 5478 421')?></p>
    			  				<p class="pedidos__font_gris"><?=InPage::__('perfil_producto_correo','eduedu@gmail.com')?></p>
    			  			</div>
    			  		
    			  		</div>
    			  	</div>			  
    			    <div class="tab-pane fade" id="direcciones" 
    			    role="tabpanel" aria-labelledby="contact-tab">
    			    	<h2 class="direcciones__title"><?=InPage::__('perfil_editar_title','Aqui podras editar la dirección de facturación.')?></h2>
    			    	<form class="" action="#!">
    			                  <div class="form-row mx-0">
    			                      <div class="col-lg-6 col-md-6 col-sm-6 direcciones__padding pl-sm-0 pr-sm-4">
    			                          <label for="nombre" class="direcciones__label"><?=InPage::__('perfil_label_nombre','Nombre')?></label>
    			                          <input type="text" id="nombre" class="form-control direcciones__input">
    			                      </div>
    			                      <div class="col-lg-6 col-md-6 col-sm-6 direcciones__padding pr-sm-0 pl-sm-4">
    			                          <label for="apellido" class="direcciones__label"><?=InPage::__('perfil_label_apellido','Apellido')?></label>
    			                          <input type="text" id="apellido" class="form-control direcciones__input">
    			                      </div>
    			                      <div class="col-lg-12 px-sm-0 direcciones__padding">
    			                          <label for="nombre_empresa"class="direcciones__label" ><?=InPage::__('perfil_label_empresa','Nombre de la empresa')?></label>
    			                          <input type="text" id="nombre_empresa" class="form-control direcciones__input">
    			                      </div>
    			                      <div class="col-lg-12 px-sm-0 direcciones__padding">
    			                        <label for="direccion" class="direcciones__label"><?=InPage::__('perfil_label_dirrecion','Dirección')?></label>
    			                        <input type="text" id="direccion" class="form-control direcciones__input">
    			                      </div>
    			                      <div class="col-lg-6 direcciones__padding col-md-6 col-sm-6 pl-sm-0 pr-sm-4">
    			                        <label for="estado" class="direcciones__label"><?=InPage::__('perfil_label_estado','Estado')?></label>
    			                        <input type="text" id="estado" class="form-control direcciones__input">
    			                      </div>
    			                      <div class="col-lg-6 direcciones__padding col-md-6 col-sm-6 pr-sm-0 pl-sm-4">
    			                        <label for="ciudad" class="direcciones__label"><?=InPage::__('perfil_label_ciudad','Ciudad')?></label>
    			                        <input type="text" id="ciudad" class="form-control direcciones__input">
    			                      </div>
    			                      <div class="col-lg-6 col-md-6 col-sm-6 direcciones__padding pl-sm-0 pr-sm-4">
    			                        <label for="tlf" class="direcciones__label"><?=InPage::__('perfil_label_tlf','Telfono')?></label>
    			                        <input type="text" id="tlf" class="form-control direcciones__input">
    			                      </div>
    			                      <div class="col-lg-6 col-md-6 col-sm-6 direcciones__padding pr-sm-0 pl-sm-4">
    			                        <label for="email" class="direcciones__label"><?=InPage::__('perfil_label_correo','Correo electrónico')?></label>
    			                        <input type="text" id="email" class="form-control direcciones__input">
    			                      </div>
    			                      <a href="#" class="direcciones__btn"><?=InPage::__('perfil_editar_btn','Regresar a la tienda')?></a>
    			                  </div>
    			        </form>
    			         
    			    </div>
    			    <div class="tab-pane fade" id="ajustes" 
    			    role="tabpanel" aria-labelledby="contact-tab">
    			    	<h2 class="ajustes__title"><?=InPage::__('perfil_ajuste_title','Aqui podras cambiar ajustes de la cuenta.')?></h2>
    			                <form class="" action="#!">
    			                  <div class="form-row mx-0">
    			                      <div class="col-lg-6 col-md-6 col-sm-6 ajustes__padding pl-sm-0 pr-sm-4">
    			                          <label for="nombre" class="ajustes__label"><?=InPage::__('perfil_label_nombre','Nombre')?></label>
    			                          <input type="text" id="nombre" class="form-control ajustes__input">
    			                      </div>
    			                      <div class="col-lg-6 col-md-6 col-sm-6 ajustes__padding pr-sm-0 pl-sm-4">
    			                          <label for="apellido" class="ajustes__label"><?=InPage::__('perfil_label_apellido','Apellido')?></label>
    			                          <input type="text" id="apellido" class="form-control ajustes__input">
    			                      </div>
    			                      <div class="col-lg-12 px-md-0 ajustes__padding">
    			                          <label for="nombre_empresa"class="ajustes__label" ><?=InPage::__('perfil_label_empresa','Nombre de la empresa')?></label>
    			                          <input type="text" id="nombre_empresa" class="form-control ajustes__input">
    			                      </div>
    			                      <div class="col-lg-6 col-md-6 col-sm-12 ajustes__padding pr-sm-4">
    			                        <label for="email" class="ajustes__label"><?=InPage::__('perfil_label_correo','Correo electrónico')?></label>
    			                        <input type="text" id="email" class="form-control ajustes__input">
    			                      </div>
    			                  </div>
    			                  <hr class="mt-5 mb-4 ajustes__line" >
    			                  <h4 class="ajustes__subtitle mt-0" ><?=InPage::__('perfil_subtitle_ajustes','Cambiar contraseña (Deja vacío en caso de no querer cambiarla)')?></h4>
    			                  <div class="form-row mx-0">
    			                    <div class="col-lg-6 col-md-6 col-sm-6 ajustes__padding pl-sm-0 pr-sm-4">
    			                          <label for="contrasena_actual" class="ajustes__label"><?=InPage::__('perfil_label_contrasena_actual','Contraseña actual')?></label>
    			                          <input type="text" id="contrasena_actual" class="form-control ajustes__input">
    			                      </div>
    			                      <div class="col-lg-6 col-md-6 col-sm-6 ajustes__padding pr-sm-0 pl-sm-4">
    			                          <label for="contrasena_nueva" class="ajustes__label"><?=InPage::__('perfil_label_contrasena_nueva','Contraseña nueva')?></label>
    			                          <input type="text" id="contrasena_nueva" class="form-control ajustes__input">
    			                      </div>
    			                      <div class="col-lg-12 col-md-12 px-md-0 ajustes__padding mb-sm-4">
    			                          <label for="confirma" class="ajustes__label"><?=InPage::__('perfil_label_confirmar_contrasena','Confirmar contraseña nueva')?></label>
    			                          <input type="text" id="confirma" class="form-control ajustes__input">
    			                      </div>
    			                  </div>
    			                </form>
    			                <a href="#" class="ajustes__btn"><?=InPage::__('perfil_btn_cambiar_contrasena','Cambiar contraseña')?></a>
    			    
    			    </div>			</div>
    		</div>
    		</div>
    	</div>
    </div>
    

<?php get_footer();?>
