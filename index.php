<?php /* Template Name: Pagina por defecto */ ?>
<?php use Opalo\Helpers\InPage; ?>
<?php get_header(); ?>

 <div class="portada__bg" style="background-image: url('<?= InPage::imgMod('home_fondo_portada','bg-home.png'); ?>')">
    	<div class="portada__rbga">
    		<div class="container__lidera">
    			<div class="portada__box-text">
    				<h2 class="portada__title"><?=InPage::__('title_portada','¡Aprovecha nuestras ofertas semanales!')?></h2>
    				<div class="d-flex flex-lg-nowrap flex-wrap portada__box-p-btn">
    					<div class="col-lg-8 col-12 px-0">
    						<p class="portada__parrafo"><?=InPage::__('texto_portada','Tus productos favoritos los ponemos al alcance de tu mano en unos cuantos click')?></p>
    					</div>
    					<div class="col-lg-4 col-12 d-flex align-items-start px-0">
    					<a href="#" class="portada__btn ml-lg-auto mx-auto mr-lg-1"><?=InPage::__('btn_portada','Ofertas Disponibles')?></a>	
    					</div>
    					
    				</div>
    			</div>
    		</div>
    	</div>
    </div>
    <div class="categ__bg">
    	<div class="container__lidera categ__container">
    		<div class="d-flex justify-content-between title__box ">
    			<h4 class="title__title"><?=InPage::__('title_section_1','Las mejores categorías')?></h4>
    			<a href="#" class="title__ver">
                    <?=InPage::__('btn_section_1','Ver todas')?>
                </a>
    		</div>		<div class="d-flex flex-wrap justify-content-center">
    			<div class="categ__col ">
    				<a class="categ__font">
    					<div class="categ__box-article text-center d-flex flex-column">
    					<span class="categ__font-icon"><i class="fas fa-bread-slice" class=""></i></span>
    					
    					<h6 class="categ__subtitle mt-auto mb-4"><?=InPage::__('title_producto_1','Alimentos Secos')?></h6>
    					</div>
    				</a>
    			</div>
    			<div class="categ__col ">
    				<a class="categ__font">
    					<div class="categ__box-article text-center d-flex flex-column">
    					<span class="categ__font-icon"><i class="fas fa-cheese"></i></span>
    					
    					<h6 class="categ__subtitle mt-auto mb-4"><?=InPage::__('title_producto_2','Lácteos')?></h6>
    					</div>
    				</a>
    			</div>
    			<div class="categ__col ">
    				<a class="categ__font">
    					<div class="categ__box-article text-center d-flex flex-column">
    					<span class="categ__font-icon"><i class="fas fa-ice-cream"></i></span>
    					
    					<h6 class="categ__subtitle mt-auto mb-4"><?=InPage::__('title_producto_3','Dulces')?>/h6>
    					</div>
    				</a>
    			</div>
    			<div class="categ__col ">
    				<a class="categ__font">
    					<div class="categ__box-article text-center d-flex flex-column">
    					<span class="categ__font-icon"><i class="fas fa-carrot"></i></span>
    					
    					<h6 class="categ__subtitle mt-auto mb-4"><?=InPage::__('title_producto_4','Verduras')?></h6>
    					</div>
    				</a>
    			</div>
    			<div class="categ__col ">
    				<a class="categ__font">
    					<div class="categ__box-article text-center d-flex flex-column">
    					<span class="categ__font-icon"><i class="fas fa-apple-alt"></i></span>
    					
    					<h6 class="categ__subtitle mt-auto mb-4"><?=InPage::__('title_producto_5','Frutas')?></h6>
    					</div>
    				</a>
    			</div>
    			<div class="categ__col ">
    				<a class="categ__font">
    					<div class="categ__box-article text-center d-flex flex-column">
    					<span class="categ__font-icon"><i class="fas fa-wine-glass-alt"></i></span>
    					
    					<h6 class="categ__subtitle mt-auto mb-4"><?=InPage::__('title_producto_6','Licores')?></h6>
    					</div>
    				</a>
    			</div>
    			<div class="categ__col ">
    				<a class="categ__font">
    					<div class="categ__box-article text-center d-flex flex-column">
    					<span class="categ__font-icon"><i class="fas fa-drumstick-bite"></i></span>
    					
    					<h6 class="categ__subtitle mt-auto mb-4"><?=InPage::__('title_producto_7','Carnes')?></h6>
    					</div>
    				</a>
    			</div>
    			<div class="categ__col ">
    				<a class="categ__font">
    					<div class="categ__box-article text-center d-flex flex-column">
    					<span class="categ__font-icon"><i class="fas fa-wine-bottle"></i></span>
    					
    					<h6 class="categ__subtitle mt-auto mb-4"><?=InPage::__('title_producto_8','Bebidas')?></h6>
    					</div>
    				</a>
    			</div>
    			<div class="categ__col ">
    				<a class="categ__font">
    					<div class="categ__box-article text-center d-flex flex-column">
    					<span class="categ__font-icon"><i class="fas fa-gift"></i></span>
    					
    					<h6 class="categ__subtitle mt-auto mb-4"><?=InPage::__('title_producto_9','Importados')?></h6>
    					</div>
    				</a>
    			</div>
    			<div class="categ__col ">
    				<a class="categ__font">
    					<div class="categ__box-article text-center d-flex flex-column">
    					<span class="categ__font-icon"><i class="fas fa-birthday-cake"></i></span>
    					
    					<h6 class="categ__subtitle mt-auto mb-4"><?=InPage::__('title_producto_10','Reposterias')?></h6>
    					</div>
    				</a>
    			</div>
    		</div>
    	</div>
    </div>
    <div class="special__bg">
    	<div class="container__lidera special__container">
    		<div class="d-flex justify-content-between title__box ">
    			<h4 class="title__title"><?=InPage::__('marcas_especiales_title','Marcas Especiales para ti')?></h4>
    			<a href="#"  class="title__ver"><?=InPage::__('marcas_especiales_btn','Ver todas')?></a>
    		</div>		
    		<div class="special__box_car">
    			<div id="carousel-example-multi" class="carousel slide carousel-multi-item v-2" data-ride="carousel">
    
    			  <!--Controls-->
    				<a class="carousel-control-prev" href="#carousel-example-multi" role="button" data-slide="prev" id="traslate_prev">
    				    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    				    <span class="sr-only">Previous</span>
    	 	 		</a>
    				<a class="carousel-control-next" href="#carousel-example-multi" id="traslate_next" role="button" data-slide="next">
    				    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    				    <span class="sr-only">Next</span>
    				</a>
    			  <!--/.Controls-->
    
    				  <div class="carousel-inner" role="listbox">
    				    <div class="carousel-item active d-flex flex-nowrap">
    				    	<div class="special__col">
    					        <div class="special__box-article d-flex flex-sm-nowrap">
    					        	<div class="col-sm-4 col-md-4 col-lg-4  px-md-1 col-12">
    					        		<div class="special__box-article-bg warning-color d-flex justify-content-center my-auto">
    					        			<img src="<?= InPage::imgMod('imagen_marca_del_producto','Maizina-Americana.png'); ?>" class="special__img" alt="">
    					        		</div>
    					        	</div>
    					        	<div class="col-sm-8 col-md-8 col-lg-8 col-12 d-flex flex-column px-0">
    					        		<h6 class="text-center special__title"><?=InPage::__('marcas_especiales_carrusel_title_1','Maizina America')?></h6>
    					        		<div class="d-flex flex-nowrap my-sm-auto mx-auto">
    					        			<div class="col-6 px-2">
    					        				<img src="<?= InPage::imgMod('imagen_del_producto_1','ImgP1.png'); ?>" class="special__mini-product" alt="">
    					        			</div>
    					        			<div class="col-6 px-2">
    					        				<img src="<?= InPage::imgMod('imagen_del_producto_2','ImgP2.png'); ?>" class="special__mini-product" class="special__mini-product" alt="">
    					        			</div>
    					        		</div>
    					        		<a class="align-self-end mt-auto special__btn" href="#"><?=InPage::__('marcas_especiales_carrusel_btn','Ver todos los productos')?></a>
    					        	</div>
    					        </div>
    				        </div>
    				        <div class="special__col">
                                <div class="special__box-article d-flex flex-sm-nowrap">
                                    <div class="col-sm-4 col-md-4 col-lg-4  px-md-1 col-12">
                                        <div class="special__box-article-bg warning-color d-flex justify-content-center my-auto">
                                            <img src="<?= InPage::imgMod('imagen_marca_del_producto','Maizina-Americana.png'); ?>" class="special__img" alt="">
                                        </div>
                                    </div>
                                    <div class="col-sm-8 col-md-8 col-lg-8 col-12 d-flex flex-column px-0">
                                        <h6 class="text-center special__title"><?=InPage::__('marcas_especiales_carrusel_title_2','Kiero')?></h6>
                                        <div class="d-flex flex-nowrap my-sm-auto mx-auto">
                                            <div class="col-6 px-2">
                                                <img src="<?= InPage::imgMod('imagen_del_producto_1','ImgP1.png'); ?>" class="special__mini-product" alt="">
                                            </div>
                                            <div class="col-6 px-2">
                                                <img src="<?= InPage::imgMod('imagen_del_producto_2','ImgP2.png'); ?>" class="special__mini-product" class="special__mini-product" alt="">
                                            </div>
                                        </div>
                                        <a class="align-self-end mt-auto special__btn" href="#"><?=InPage::__('marcas_especiales_carrusel_btn','Ver todos los productos')?></a>
                                    </div>
                                </div>
                            </div>
                            <div class="special__col">
                                <div class="special__box-article d-flex flex-sm-nowrap">
                                    <div class="col-sm-4 col-md-4 col-lg-4  px-md-1 col-12">
                                        <div class="special__box-article-bg warning-color d-flex justify-content-center my-auto">
                                            <img src="<?= InPage::imgMod('imagen_marca_del_producto','Maizina-Americana.png'); ?>" class="special__img" alt="">
                                        </div>
                                    </div>
                                    <div class="col-sm-8 col-md-8 col-lg-8 col-12 d-flex flex-column px-0">
                                        <h6 class="text-center special__title"><?=InPage::__('marcas_especiales_carrusel_title_3','Naturalista')?></h6>
                                        <div class="d-flex flex-nowrap my-sm-auto mx-auto">
                                            <div class="col-6 px-2">
                                                <img src="<?= InPage::imgMod('imagen_del_producto_1','ImgP1.png'); ?>" class="special__mini-product" alt="">
                                            </div>
                                            <div class="col-6 px-2">
                                                <img src="<?= InPage::imgMod('imagen_del_producto_2','ImgP2.png'); ?>" class="special__mini-product" class="special__mini-product" alt="">
                                            </div>
                                        </div>
                                        <a class="align-self-end mt-auto special__btn" href="#"><?=InPage::__('marcas_especiales_carrusel_btn','Ver todos los productos')?></a>
                                    </div>
                                </div>
                            </div>
                            <div class="special__col">
                                <div class="special__box-article d-flex flex-sm-nowrap">
                                    <div class="col-sm-4 col-md-4 col-lg-4  px-md-1 col-12">
                                        <div class="special__box-article-bg warning-color d-flex justify-content-center my-auto">
                                            <img src="<?= InPage::imgMod('imagen_marca_del_producto','Maizina-Americana.png'); ?>" class="special__img" alt="">
                                        </div>
                                    </div>
                                    <div class="col-sm-8 col-md-8 col-lg-8 col-12 d-flex flex-column px-0">
                                        <h6 class="text-center special__title"><?=InPage::__('marcas_especiales_carrusel_title_1','Maizina America')?></h6>
                                        <div class="d-flex flex-nowrap my-sm-auto mx-auto">
                                            <div class="col-6 px-2">
                                                <img src="<?= InPage::imgMod('imagen_del_producto_1','ImgP1.png'); ?>" class="special__mini-product" alt="">
                                            </div>
                                            <div class="col-6 px-2">
                                                <img src="<?= InPage::imgMod('imagen_del_producto_2','ImgP2.png'); ?>" class="special__mini-product" class="special__mini-product" alt="">
                                            </div>
                                        </div>
                                        <a class="align-self-end mt-auto special__btn" href="#"><?=InPage::__('marcas_especiales_carrusel_btn','Ver todos los productos')?></a>
                                    </div>
                                </div>
                            </div>
                            <div class="special__col">
                                <div class="special__box-article d-flex flex-sm-nowrap">
                                    <div class="col-sm-4 col-md-4 col-lg-4  px-md-1 col-12">
                                        <div class="special__box-article-bg warning-color d-flex justify-content-center my-auto">
                                            <img src="<?= InPage::imgMod('imagen_marca_del_producto','Maizina-Americana.png'); ?>" class="special__img" alt="">
                                        </div>
                                    </div>
                                    <div class="col-sm-8 col-md-8 col-lg-8 col-12 d-flex flex-column px-0">
                                        <h6 class="text-center special__title"><?=InPage::__('marcas_especiales_carrusel_title_2','Kiero')?></h6>
                                        <div class="d-flex flex-nowrap my-sm-auto mx-auto">
                                            <div class="col-6 px-2">
                                                <img src="<?= InPage::imgMod('imagen_del_producto_1','ImgP1.png'); ?>" class="special__mini-product" alt="">
                                            </div>
                                            <div class="col-6 px-2">
                                                <img src="<?= InPage::imgMod('imagen_del_producto_2','ImgP2.png'); ?>" class="special__mini-product" class="special__mini-product" alt="">
                                            </div>
                                        </div>
                                        <a class="align-self-end mt-auto special__btn" href="#"><?=InPage::__('marcas_especiales_carrusel_btn','Ver todos los productos')?></a>
                                    </div>
                                </div>
                            </div>
                            <div class="special__col">
                                <div class="special__box-article d-flex flex-sm-nowrap">
                                    <div class="col-sm-4 col-md-4 col-lg-4  px-md-1 col-12">
                                        <div class="special__box-article-bg warning-color d-flex justify-content-center my-auto">
                                            <img src="<?= InPage::imgMod('imagen_marca_del_producto','Maizina-Americana.png'); ?>" class="special__img" alt="">
                                        </div>
                                    </div>
                                    <div class="col-sm-8 col-md-8 col-lg-8 col-12 d-flex flex-column px-0">
                                        <h6 class="text-center special__title"><?=InPage::__('marcas_especiales_carrusel_title_3','Naturalista')?></h6>
                                        <div class="d-flex flex-nowrap my-sm-auto mx-auto">
                                            <div class="col-6 px-2">
                                                <img src="<?= InPage::imgMod('imagen_del_producto_1','ImgP1.png'); ?>" class="special__mini-product" alt="">
                                            </div>
                                            <div class="col-6 px-2">
                                                <img src="<?= InPage::imgMod('imagen_del_producto_2','ImgP2.png'); ?>" class="special__mini-product" class="special__mini-product" alt="">
                                            </div>
                                        </div>
                                        <a class="align-self-end mt-auto special__btn" href="#"><?=InPage::__('marcas_especiales_carrusel_btn','Ver todos los productos')?></a>
                                    </div>
                                </div>
                            </div>
    				     </div>
    				  </div>
    
    			  </div>
    
    			</div>
    		</div>
    
    		</div>
    	</div>
    	
    </div>
    </div>
       
    <div class="destacados__bg">
    	<div class="container__lidera destacados__container">
    		<div class="d-flex justify-content-between title__box ">
    			<h4 class="title__title"><?=InPage::__('title_seccion_2','Productos Destacados')?></h4>
    			<a href="#"  class="title__ver"></a>
    		</div>
    		<div id="carousel-example-multi" class="carousel slide carousel-multi-item v-2" data-ride="carousel">
    		
    			<!--Controls-->
    			<a class="carousel-control-prev" href="#carusel-2" role="button" data-slide="prev" id="">
    				<span class="carousel-control-prev-icon" aria-hidden="true"></span>
    				<span class="sr-only">Previous</span>
    			</a>
    			<a class="carousel-control-next" href="#carusel-2" id="" role="button" data-slide="next">
    				<span class="carousel-control-next-icon" aria-hidden="true"></span>
    				<span class="sr-only">Next</span>
    			</a>
    						 
    			<div class="carousel-inner" role="listbox">
    				<div class="carousel-item active d-flex flex-nowrap">
    					<div class="col-lg-3 col-md-4 col-sm-6 col-12 carrusel__padding_destacados">
    						<div class="carrusel__box_destacados d-flex flex-column" data-toggle="modal" data-target="#producto">
    							<div class="carrusel__box-img_destacados mx-auto">
    								<img src="<?= InPage::imgMod('home_carrusel_producto_destacados','Maizina-Americana.png'); ?>" class="carrusel__img" alt="">
    							</div>
    							<h6 class="text-center carrusel__title_destacados"><?=InPage::__('title_carrusel_seccion_2','Maizina 900grs')?></h6>
    							<span class="text-center carrusel__subtitle_destacados"><?=InPage::__('precio_carrusel_seccion_2','Bs. 250.000.00')?></span>
    							<a href="#agregando" class="carrusel__btn_agregar" id="mostrar"><i class="fa fa-shopping-cart" aria-hidden="true"></i> <?=InPage::__('title_carrusel_seccion_2','Agregar')?></a>
    							<div  class="d-flex flex-nowrap mt-auto carrusel__btn-show">
    								<a href="#salir" class="carrusel__btn_salir"><i class="fa fa-times" aria-hidden="true"></i></a>
    								<a href="#restar" class="carrusel__btn_restar carrusel__btn_otro"><i class="fa fa-minus" aria-hidden="true"></i></a>
    								<span href="#valor" class="carrusel__numero carrusel__btn_otro">1</span>
    								<a href="#sumar" class="carrusel__btn_sumar carrusel__btn_otro"><i class="fa fa-plus" aria-hidden="true"></i></a>
    							</div>
    						</div>
    					</div>
    					<div class="col-lg-3 col-md-4 col-sm-6 col-12 carrusel__padding_destacados">
                            <div class="carrusel__box_destacados d-flex flex-column" data-toggle="modal" data-target="#producto">
                                <div class="carrusel__box-img_destacados mx-auto">
                                    <img src="<?= InPage::imgMod('home_carrusel_producto_destacados','Maizina-Americana.png'); ?>" class="carrusel__img" alt="">
                                </div>
                                <h6 class="text-center carrusel__title_destacados"><?=InPage::__('title_carrusel_seccion_2','Maizina 900grs')?></h6>
                                <span class="text-center carrusel__subtitle_destacados"><?=InPage::__('precio_carrusel_seccion_2','Bs. 250.000.00')?></span>
                                <a href="#agregando" class="carrusel__btn_agregar" id="mostrar"><i class="fa fa-shopping-cart" aria-hidden="true"></i> <?=InPage::__('title_carrusel_seccion_2','Agregar')?></a>
                                <div  class="d-flex flex-nowrap mt-auto carrusel__btn-show">
                                    <a href="#salir" class="carrusel__btn_salir"><i class="fa fa-times" aria-hidden="true"></i></a>
                                    <a href="#restar" class="carrusel__btn_restar carrusel__btn_otro"><i class="fa fa-minus" aria-hidden="true"></i></a>
                                    <span href="#valor" class="carrusel__numero carrusel__btn_otro">1</span>
                                    <a href="#sumar" class="carrusel__btn_sumar carrusel__btn_otro"><i class="fa fa-plus" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-6 col-12 carrusel__padding_destacados">
                            <div class="carrusel__box_destacados d-flex flex-column" data-toggle="modal" data-target="#producto">
                                <div class="carrusel__box-img_destacados mx-auto">
                                    <img src="<?= InPage::imgMod('home_carrusel_producto_destacados','Maizina-Americana.png'); ?>" class="carrusel__img" alt="">
                                </div>
                                <h6 class="text-center carrusel__title_destacados"><?=InPage::__('title_carrusel_seccion_2','Maizina 900grs')?></h6>
                                <span class="text-center carrusel__subtitle_destacados"><?=InPage::__('precio_carrusel_seccion_2','Bs. 250.000.00')?></span>
                                <a href="#agregando" class="carrusel__btn_agregar" id="mostrar"><i class="fa fa-shopping-cart" aria-hidden="true"></i> <?=InPage::__('title_carrusel_seccion_2','Agregar')?></a>
                                <div  class="d-flex flex-nowrap mt-auto carrusel__btn-show">
                                    <a href="#salir" class="carrusel__btn_salir"><i class="fa fa-times" aria-hidden="true"></i></a>
                                    <a href="#restar" class="carrusel__btn_restar carrusel__btn_otro"><i class="fa fa-minus" aria-hidden="true"></i></a>
                                    <span href="#valor" class="carrusel__numero carrusel__btn_otro">1</span>
                                    <a href="#sumar" class="carrusel__btn_sumar carrusel__btn_otro"><i class="fa fa-plus" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-6 col-12 carrusel__padding_destacados">
                            <div class="carrusel__box_destacados d-flex flex-column" data-toggle="modal" data-target="#producto">
                                <div class="carrusel__box-img_destacados mx-auto">
                                    <img src="<?= InPage::imgMod('home_carrusel_producto_destacados','Maizina-Americana.png'); ?>" class="carrusel__img" alt="">
                                </div>
                                <h6 class="text-center carrusel__title_destacados"><?=InPage::__('title_carrusel_seccion_2','Maizina 900grs')?></h6>
                                <span class="text-center carrusel__subtitle_destacados"><?=InPage::__('precio_carrusel_seccion_2','Bs. 250.000.00')?></span>
                                <a href="#agregando" class="carrusel__btn_agregar" id="mostrar"><i class="fa fa-shopping-cart" aria-hidden="true"></i> <?=InPage::__('title_carrusel_seccion_2','Agregar')?></a>
                                <div  class="d-flex flex-nowrap mt-auto carrusel__btn-show">
                                    <a href="#salir" class="carrusel__btn_salir"><i class="fa fa-times" aria-hidden="true"></i></a>
                                    <a href="#restar" class="carrusel__btn_restar carrusel__btn_otro"><i class="fa fa-minus" aria-hidden="true"></i></a>
                                    <span href="#valor" class="carrusel__numero carrusel__btn_otro">1</span>
                                    <a href="#sumar" class="carrusel__btn_sumar carrusel__btn_otro"><i class="fa fa-plus" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-6 col-12 carrusel__padding_destacados">
                            <div class="carrusel__box_destacados d-flex flex-column" data-toggle="modal" data-target="#producto">
                                <div class="carrusel__box-img_destacados mx-auto">
                                    <img src="<?= InPage::imgMod('home_carrusel_producto_destacados','Maizina-Americana.png'); ?>" class="carrusel__img" alt="">
                                </div>
                                <h6 class="text-center carrusel__title_destacados"><?=InPage::__('title_carrusel_seccion_2','Maizina 900grs')?></h6>
                                <span class="text-center carrusel__subtitle_destacados"><?=InPage::__('precio_carrusel_seccion_2','Bs. 250.000.00')?></span>
                                <a href="#agregando" class="carrusel__btn_agregar" id="mostrar"><i class="fa fa-shopping-cart" aria-hidden="true"></i> <?=InPage::__('title_carrusel_seccion_2','Agregar')?></a>
                                <div  class="d-flex flex-nowrap mt-auto carrusel__btn-show">
                                    <a href="#salir" class="carrusel__btn_salir"><i class="fa fa-times" aria-hidden="true"></i></a>
                                    <a href="#restar" class="carrusel__btn_restar carrusel__btn_otro"><i class="fa fa-minus" aria-hidden="true"></i></a>
                                    <span href="#valor" class="carrusel__numero carrusel__btn_otro">1</span>
                                    <a href="#sumar" class="carrusel__btn_sumar carrusel__btn_otro"><i class="fa fa-plus" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
    				</div>
    			</div>
    		
    		</div>	</div>
    </div>
     <!-- Central Modal Small -->
        <div class="modal fade productos__bg" id="producto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
              aria-hidden="true">
    
              <!-- Change class .modal-sm to change the size of the modal -->
            <div class="modal-dialog modal-dialog-centered container__lidera productos__container" role="document">
    
    
                <div class="modal-content productos__modal_body"> 
                  <div class="modal-body">
                    <div class="d-flex justify-content-end">
                      <a href="#" class="ml-auto productos__btn_salir" data-dismiss="modal" aria-label="Close"><i class="fas fa-times"></i></a>
                    </div>
                    <div class="d-flex flex-wrap">
                      <div class="col-12 col-md-12 col-lg-6 productos__padding">
                        <div href="#produc" class="productos__box_imgs">
                          <img src="<?= InPage::imgMod('imagen_marca_del_producto','Maizina-Americana.png'); ?>" class="productos__imgs" alt="">
                        </div>
                      </div>
                      <div class="col-12 col-md-12 col-lg-6 productos__padding">
                        <h5 class="productos__title_proct mb-1"><?=InPage::__('title_modal_carrusel_seccion_2','title_modal_carrusel_seccion_2')?></h5>
                        <hr class="mb-1 mt-0">
                        <p class="productos__font productos__descrip mb-2"><?=InPage::__('description_modal_carrusel_seccion_2','Almidón de maíz o maicena, harina para hacer pan, pastas, bizcochos, bases de pizza.<br> Producto blanco, en polvo, muy suave y refinado, extraído de los mejores granos de maíz.')?></p>
                        <hr class="my-2">
                        <h6 class="productos__font productos__detalles"><?=InPage::__('detalles_modal_1','Disponible 70 unidades')?><h6>
                        <h6 class="productos__font productos__detalles"><?=InPage::__('detalles_modal_2','SKU:PDD-22062020')?></h6>
                        <h6 class="productos__font productos__detalles"><?=InPage::__('detalles_modal_3','Another important info #1: Lorem')?></h6>
                        <h6 class="productos__font productos__detalles"><?=InPage::__('detalles_modal_4','Another important info #2: Lorem')?></h6>
                        <hr class="my-2">
                        <div class="d-flex justify-content-sm-between productos__movil_colum">
                          <h5 class="productos__precios my-auto"><?=InPage::__('modal_precio','Precio: 220,000.00 Bs')?></h5>
                          <a href="#comprado__sorry" class="productos__btn_agregar"><i class="fa fa-shopping-cart" aria-hidden="true"></i> <?=InPage::__('btn_precio','Agregar')?></a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
    <!-- Central Modal Small -->

    <a class="navbar-brand dolar__btn ml-auto fixed-bottom" href="#"><i class="fas fa-dollar-sign"></i></a>

<?php get_footer(); ?>
