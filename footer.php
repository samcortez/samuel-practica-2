<?php use Opalo\Helpers\InPage; ?>

    <footer class="footer__bg" style="background-image: url('<?= InPage::imgMod('footer_fondo','contact.png'); ?>')">
    	<div class="container__lidera footer__container pb-5">
    		<div class="footer__box">
    			<h4 class="footer__title"><?=InPage::__('title_footer','¡Hola! ¿en qué podemos ayudarte?')?></h4>
    			<div class="row">
    			<form class="col-md-6 col-12">
    		      <input type="text" id="defaultFormCardNameEx" placeholder="Tu nombre" class="form-control footer__inputs">
    		      <input type="email" id="defaultFormCardEmailEx" placeholder="Correo electronico" class="form-control footer__inputs">
    		      <input type="text" id="defaultFormCardNameEx" placeholder="Telf. (opcional)" class="form-control footer__inputs">
        		</form>
        		<form class="col-md-6 col-12">
    				<textarea class="form-control footer__textarea" placeholder="Mensaje" id="exampleFormControlTextarea1"></textarea>
    		
    		      <button type="button" class="btn  btn-block footer__btn"><?=InPage::__('btn_footer','Enviar Mensaje?')?></button>
        		</form>
    			</div>
    		</div>
    		<div class="footer__box_2 d-flex flex-wrap">
    		<div class="col-lg col-md text-md-left text-center footer__padding">
    			<h5 class="footer__font footer__title_contac"><?=InPage::__('subtitle_footer_1','¡Conoce más de nosotros!')?></h5>
    			<ul class="list-unstyled">
    	          <li class="nav-item my- pl-md-3">
    	            <a href="#!" class="footer__link"><i class="fa fa-plus" aria-hidden="true"></i> <?=InPage::__('link_footer_1','Historias')?></a>
    	          </li>
    	          <li class="nav-item my- pl-md-3">
    	            <a href="#!" class="footer__link"><i class="fa fa-plus" aria-hidden="true"></i> <?=InPage::__('link_footer_2','Filosofía de trabajo')?></a>
    	          </li>
    	          <li class="nav-item my- pl-md-3">
    	            <a href="#!" class="footer__link"><i class="fa fa-plus" aria-hidden="true"></i> <?=InPage::__('link_footer_3','Información para el negocios')?></a>
    	          </li>
    	        </ul>
    		</div>
    		<div class="footer__line-1"></div>
    		<div class="col-lg col-md d-flex flex-column justify-content-center footer__padding">
    			<img src="<?= InPage::imgMod('footer_icon_lidera','logo-blanco-lidera.png'); ?>" class="footer__img_lidera mx-auto" alt="">
    			<a href="#nextscale" class="footer__box_nextscale mx-auto" >Creado por 
    			<img src="<?= InPage::imgMod('footer_icon_nexscale','logo-blanco.png'); ?>" class="footer__img_nextscale" alt="">
    			</a>
    		</div>
    		<div class="footer__line-2"></div>		
    		<div class="col-lg col-md-12 mt-lg-0 mt-4 justify-content-center footer__padding">
    			<h5 class="footer__font footer__title_ubi"><?=InPage::__('subtitle_footer_2','Ubicanos en...')?></h5>
    			<p class="footer__font mb-0 footer__parrafo  pl-lg-4 text-center"><i class="fa fa-circle" aria-hidden="true"></i> <?=InPage::__('footer_ubicacion_texto','Av. Principal Local/Galpón 280801 Sector Parque Industrial Los Pinos.
                Puerto Ordaz, Ciudad Guayana - Edo.Bolivar')?></p>
    			<p class="footer__font mb-0 footer__parrafo mt-1  pl-lg-4 text-center"><i class="fa fa-circle" aria-hidden="true"></i> <?=InPage::__('numero_footer','No. de Contacto: +58 0286-994 8711')?></p>
    		</div>
    		</div>
    	</div>
    </footer>

    <?php wp_footer(); ?>
  </body>
</html>
