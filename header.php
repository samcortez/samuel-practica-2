<!DOCTYPE html>
<?php use Opalo\Helpers\InPage; ?>
<html <?php language_attributes(); ?>>

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <meta http-equiv="X-UA-Compatible" content="ie=edge" />
  <!-- Meta tags here! -->
  <title><?= InPage::pageTitle(); ?></title>
  <meta name="description" content="<?php bloginfo('description'); ?>" />
  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
  <?= InPage::noticesHandler(); ?>

   <!--Navbar-->
    <nav class="navbar navbar-expand-lg navbar-dark bg-white p-0 d-flex flex-wrap fixed-top">
      <div class="container__lidera navbar-collapse d-flex flex-nowrap">
            <!-- Navbar brand -->
            <a class="navbar-brand" href="#"><img src="<?= InPage::imgMod('navbar_logo','logo-blanco.png'); ?>" class="navbar__img-libera" alt="" /></a>
    
            <!-- Collapse button -->
            
            
            <!-- Collapsible content -->
         
              <div class="navbar__line_vertical navbar__hidden"></div>
              <!-- Links -->
              <ul class="navbar-nav navbar__hidden">
                <!-- Dropdown -->
                <li class="nav-item dropdown mx-2 px-1">
                  <a class="nav-link dropdown-toggle navbar__btn" id="navbarDropdownMenuLink" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false"><i class="fa fa-list" aria-hidden="true"></i><?=InPage::__('home_categorias_title','Categorias')?>
                  </a>
                  <div class="dropdown-menu dropdown-primary py-0 navbar__box-item" aria-labelledby="navbarDropdownMenuLink">
                    <ul class="navbar__hover-item-2 position-relative"> <a class="dropdown-item d-flex flex-nowrap navbar__border px-0 navbar__font-item navbar__hover-item" href="#"><div class="navbar__circulo"></div><?=InPage::__('home_categorias_1','Alimentos')?></a>
                      <ul class="navbar-nav navbar__box-item-2 flex-column">
                        <li class="nav-item navbar__box-item-li"><a class="navbar__item-2 px-0 navbar__border-2 d-flex flex-nowrap"><div class="navbar__circulo-2"></div><?=InPage::__('home_categorias_1_1','Bebidas')?></a></li>
                        <li class="nav-item navbar__box-item-li"><a class="navbar__item-2 px-0 navbar__border-2 d-flex flex-nowrap"><div class="navbar__circulo-2"></div><?=InPage::__('home_categorias_1_2','Café Te Infusiones')?></a></li>
                        <li class="nav-item navbar__box-item-li"><a class="navbar__item-2 px-0 navbar__border-2 d-flex flex-nowrap"><div class="navbar__circulo-2"></div><?=InPage::__('home_categorias_1_3','Especies')?></a></li>
                        <li class="nav-item navbar__box-item-li"><a class="navbar__item-2 px-0 navbar__border-2 d-flex flex-nowrap"><div class="navbar__circulo-2"></div><?=InPage::__('home_categorias_1_4','Cereales')?></a></li>
                        <li class="nav-item navbar__box-item-li"><a class="navbar__item-2 px-0 navbar__border-2 d-flex flex-nowrap"><div class="navbar__circulo-2"></div><?=InPage::__('home_categorias_1_5','Snack')?></a></li>
                        <li class="nav-item navbar__box-item-li"><a class="navbar__item-2 px-0 navbar__border-2 d-flex flex-nowrap"><div class="navbar__circulo-2"></div><?=InPage::__('home_categorias_1_6','Salsa')?></a></li>
                        <li class="nav-item navbar__box-item-li"><a class="navbar__item-2 px-0 navbar__border-2 d-flex flex-nowrap"><div class="navbar__circulo-2"></div><?=InPage::__('home_categorias_1_7','Alimentos Enlatados')?></a></li>
                        <li class="nav-item navbar__box-item-li"><a class="navbar__item-2 px-0 navbar__border-2 d-flex flex-nowrap"><div class="navbar__circulo-2"></div><?=InPage::__('home_categorias_1_8','Reposteria')?></a></li>
                        <li class="nav-item navbar__box-item-li"><a class="navbar__item-2 px-0 navbar__border-2 d-flex flex-nowrap"><div class="navbar__circulo-2"></div><?=InPage::__('home_categorias_1_10','Aceites y Vinagres')?></a></li>
                        <li class="nav-item navbar__box-item-li"><a class="navbar__item-2 px-0 navbar__border-2 d-flex flex-nowrap"><div class="navbar__circulo-2"></div><?=InPage::__('home_categorias_1_11','Mermeladas y gelatinas')?></a></li>
                        <li class="nav-item navbar__box-item-li"><a class="navbar__item-2 px-0 navbar__border-2 d-flex flex-nowrap"><div class="navbar__circulo-2"></div><?=InPage::__('home_categorias_1_12','Pasta y arroces')?></a></li>
                        <li class="nav-item navbar__box-item-li"><a class="navbar__item-2 px-0 navbar__border-2 d-flex flex-nowrap"><div class="navbar__circulo-2"></div><?=InPage::__('home_categorias_1_13','Untables')?></a></li>
                        <li class="nav-item navbar__box-item-li"><a class="navbar__item-2 px-0 navbar__border-2 d-flex flex-nowrap"><div class="navbar__circulo-2"></div><?=InPage::__('home_categorias_1_13','Endulzables')?></a></li>
                      </ul>
                    </ul>
                    <ul class="navbar__hover-item-2 position-relative"><a class="dropdown-item d-flex flex-nowrap navbar__border px-0 navbar__font-item navbar__hover-item" href="#"><div class="navbar__circulo"></div><?=InPage::__('home_categorias_2','Charcuteria')?></a>
                      <ul class="navbar-nav navbar__box-item-2 flex-column">
                        
                      </ul>
                    </ul>
                    <ul class="navbar__hover-item-2 position-relative"><a class="dropdown-item d-flex flex-nowrap navbar__border px-0 navbar__font-item navbar__hover-item" href="#"><div class="navbar__circulo"></div><?=InPage::__('home_categorias_3','Cofiteria')?></a>
                      <ul class="navbar-nav navbar__box-item-2 flex-column">
                      </ul>
                    </ul>
                    <ul class="navbar__hover-item-2 position-relative"><a class="dropdown-item d-flex flex-nowrap navbar__border px-0 navbar__font-item navbar__hover-item" href="#"><div class="navbar__circulo"></div><?=InPage::__('home_categorias_4','Cuidado del Bebé')?></a>
                      <ul class="navbar-nav navbar__box-item-2 flex-column">
                       
                      </ul>
                    </ul>
                    <ul class="navbar__hover-item-2 position-relative"><a class="dropdown-item d-flex flex-nowrap navbar__border px-0 navbar__font-item navbar__hover-item" href="#"><div class="navbar__circulo"></div><?=InPage::__('home_categorias_5','Cuidado Personal')?></a>
                      <ul class="navbar-nav navbar__box-item-2 flex-column">
                        <li class="nav-item navbar__box-item-li"><a class="navbar__item-2 px-0 navbar__border-2 d-flex flex-nowrap"><div class="navbar__circulo-2"></div><?=InPage::__('home_categorias_5_1','Protección Femenina')?></a></li>
                        <li class="nav-item navbar__box-item-li"><a class="navbar__item-2 px-0 navbar__border-2 d-flex flex-nowrap"><div class="navbar__circulo-2"></div><?=InPage::__('home_categorias_5_2','Desodorantes')?></a></li>
                        <li class="nav-item navbar__box-item-li"><a class="navbar__item-2 px-0 navbar__border-2 d-flex flex-nowrap"><div class="navbar__circulo-2"></div><?=InPage::__('home_categorias_5_3','Afeitados')?></a></li>
                        <li class="nav-item navbar__box-item-li"><a class="navbar__item-2 px-0 navbar__border-2 d-flex flex-nowrap"><div class="navbar__circulo-2"></div><?=InPage::__('home_categorias_5_4','Cuidado de la piel')?></a></li>
                        <li class="nav-item navbar__box-item-li"><a class="navbar__item-2 px-0 navbar__border-2 d-flex flex-nowrap"><div class="navbar__circulo-2"></div><?=InPage::__('home_categorias_5_5','Cuidado Bocal')?></a></li>
                        <li class="nav-item navbar__box-item-li"><a class="navbar__item-2 px-0 navbar__border-2 d-flex flex-nowrap"><div class="navbar__circulo-2"></div><?=InPage::__('home_categorias_5_6','Toallitas humedas')?></a></li>
                        <li class="nav-item navbar__box-item-li"><a class="navbar__item-2 px-0 navbar__border-2 d-flex flex-nowrap"><div class="navbar__circulo-2"></div><?=InPage::__('home_categorias_5_7','Jabon de Tocador')?></a></li>
                        <li class="nav-item navbar__box-item-li"><a class="navbar__item-2 px-0 navbar__border-2 d-flex flex-nowrap"><div class="navbar__circulo-2"></div><?=InPage::__('home_categorias_5_8','Jabon liquido')?></a></li>
                      </ul>
                    </ul>
                    <ul class="navbar__hover-item-2 position-relative"><a class="dropdown-item d-flex flex-nowrap navbar__border px-0 navbar__font-item navbar__hover-item" href="#"><div class="navbar__circulo"></div><?=InPage::__('home_categorias_6','Limpieza')?></a>
                      <ul class="navbar-nav navbar__box-item-2 flex-column">
                        <li class="nav-item navbar__box-item-li"><a class="navbar__item-2 px-0 navbar__border-2 d-flex flex-nowrap"><div class="navbar__circulo-2"></div><?=InPage::__('home_categorias_6_1','Detergente')?></a></li>
                        <li class="nav-item navbar__box-item-li"><a class="navbar__item-2 px-0 navbar__border-2 d-flex flex-nowrap"><div class="navbar__circulo-2"></div><?=InPage::__('home_categorias_6_2','Antibacteriales')?></a></li>
                        <li class="nav-item navbar__box-item-li"><a class="navbar__item-2 px-0 navbar__border-2 d-flex flex-nowrap"><div class="navbar__circulo-2"></div><?=InPage::__('home_categorias_6_3','Cloros')?></a></li>
                        <li class="nav-item navbar__box-item-li"><a class="navbar__item-2 px-0 navbar__border-2 d-flex flex-nowrap"><div class="navbar__circulo-2"></div><?=InPage::__('home_categorias_6_4','Limpia vidrios')?></a></li>
                      </ul>
                    </ul>
                    <ul class="navbar__hover-item-2 position-relative"><a class="dropdown-item d-flex flex-nowrap navbar__border px-0 navbar__font-item navbar__hover-item" href="#"><div class="navbar__circulo"></div><?=InPage::__('home_categorias_7','Mascotas')?></a>
                      <ul class="navbar-nav navbar__box-item-2 flex-column">
                        <li class="nav-item navbar__box-item-li"><a class="navbar__item-2 px-0 navbar__border-2 d-flex flex-nowrap"><div class="navbar__circulo-2"></div><?=InPage::__('home_categorias_7_1','Perros')?></a></li>
                        <li class="nav-item navbar__box-item-li"><a class="navbar__item-2 px-0 navbar__border-2 d-flex flex-nowrap"><div class="navbar__circulo-2"></div><?=InPage::__('home_categorias_7_2','Gatos')?></a></li>
                      </ul>
                    </ul>
                    <ul class="navbar__hover-item-2 position-relative"><a class="dropdown-item d-flex flex-nowrap navbar__border px-0 navbar__font-item navbar__hover-item" href="#"><div class="navbar__circulo"></div><?=InPage::__('home_categorias_8','Papel')?></a>
                      <ul class="navbar-nav navbar__box-item-2 flex-column">
                        <li class="nav-item navbar__box-item-li"><a class="navbar__item-2 px-0 navbar__border-2 d-flex flex-nowrap"><div class="navbar__circulo-2"></div><?=InPage::__('home_categorias_8_1','Papel toilet')?></a></li>
                        <li class="nav-item navbar__box-item-li"><a class="navbar__item-2 px-0 navbar__border-2 d-flex flex-nowrap"><div class="navbar__circulo-2"></div><?=InPage::__('home_categorias_8_2','Papel absorbente')?></a></li>
                      </ul>
                    </ul>
                    <ul class="navbar__hover-item-2 position-relative"><a class="dropdown-item d-flex flex-nowrap px-0 navbar__font-item navbar__hover-item" href="#"><div class="navbar__circulo"></div>Pilas Alcalinas</a>
                      <ul class="navbar-nav navbar__box-item-2 flex-column">
                       
                      </ul>
                    </ul>
                  </div>
                </li>
              </ul>
              <div class="navbar__line_vertical navbar__hidden"></div>
            
              <!-- Links -->
              
              <form class="form-row navbar__form_search mx-lg-2 d-flex justify-content-center px-lg-1 mx-0">
                <div class="p-0 position-relative navbar__box-search">
                  <input class="form-control navbar__search" type="text" placeholder="Buscar productos..." aria-label="Search">
                  <img class="navbar__img-icon-search" src="<?= InPage::imgMod('icono_search','lupa.png'); ?>" alt="">
                </div>
              </form>
              
            
            
              <div class="navbar__line_vertical navbar__hidden"></div>
              <ul class="navbar-nav">
                <!-- Dropdown -->
                <li class="nav-item dropdown mx-2 px-1 navbar__hidden">
                  <a class="nav-link navbar__btn-acceder"><i class="fa fa-shopping-cart" aria-hidden="true"></i> <?=InPage::__('home_btnnav_1','Carrito')?></a>
                </li>
              </ul>
    
              <div class="navbar__line_vertical navbar__hidden"></div>
              <ul class="navbar-nav navbar__hidden">
                <!-- Dropdown -->
                <li class="nav-item dropdown ml-2 pl-1">
                  <a class="nav-link navbar__btn-cuenta"><?=InPage::__('home_btnnav_2','Cuenta')?></a>
                </li>
              </ul>
    
         
              <!-- Collapsible content -->
              <button class="navbar-toggler bg-transparent ml-2 px-2" type="button" data-toggle="collapse" data-target="#basicExampleNav"
              aria-controls="basicExampleNav" aria-expanded="false" aria-label="Toggle navigation" id="btn_hamburger">
              <div class="navbar__toggler-icon-box">
                <span class="navbar__toggler-icon-line"></span>
                <span class="navbar__toggler-icon-line"></span>
                <span class="navbar__toggler-icon-line"></span>
                <span class="navbar__toggler-icon-line"></span>
              </div>
            </button>
    
      </div>
    
      <div class="collapse navbar__container-login py-0 navbar__box-login" id="basicExampleNav">
        <div class="navbar-collapse d-flex flex-wrap navbar__box-login pt-3 pb-4" id="basicExampleNav">
              <div class="d-flex flex-nowrap">
                <div class="navbar__box-img-person mb-sm-3 mb-1 navbar__padding_zero">
                    <img src="<?= InPage::imgMod('icono_persona','person.png'); ?>" class="navbar__img-person" alt="">
                  </div>
                <div class="navbar__box-text-img">
                  <h4 class="navbar__font-login navbar__login-title"><?=InPage::__('home_movil_title','Hola')?></h4>
                  <p class="navbar__font-login mb-0 navbar__login-parrafo"><?=InPage::__('home_movil_texto','Inicia sesión o crea tu cuenta para empezar a compara con unos cuantos clicks')?></p>
                </div>
              </div>
                <div class="col-sm-6 col-12 py-1 navbar__padding_zero">
                  <button type="button" class="navbar__btn-login navbar__primary btn btn-block"><?=InPage::__('home_movil_btn_1','Iniciar Sesión')?></button>
                </div>
                <div class="col-sm-6 col-12 py-1 navbar__padding_zero">
                  <button type="button" class="navbar__btn-login navbar__gris btn btn-block"><?=InPage::__('home_movil_btn_2','Registrar una Cuenta')?></button>
                </div>
                  <hr class="w-100 mb-0 mt-sm-5 mt-4">
                  <a href="#" class="navbar__font-login col-12 navbar__hipervinculo navbar__padding_zero"><i class="fa fa-home" aria-hidden="true"></i> <?=InPage::__('home_movil_menu_1','Inicio')?></a>
                  <hr class="w-100 my-0">
                  <a href="#" class="navbar__font-login col-12 navbar__hipervinculo navbar__padding_zero"><i class="fa fa-list" aria-hidden="true"></i> <?=InPage::__('home_movil_menu_2','Categorias')?></a>
                  <hr class="w-100 my-0">
                  <a href="#" class="navbar__font-login col-12 navbar__hipervinculo navbar__padding_zero"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=InPage::__('home_movil_menu_3','Ayuda')?></a>
                  <hr class="w-100 my-0">
                  <a href="#" class="navbar__font-login col-12 navbar__hipervinculo navbar__padding_zero"><i class="fas fa-comment-alt"></i> <?=InPage::__('home_movil_menu_4','Contactanos')?></a>
                  <hr class="w-100 my-0">
                  <a href="#" class="navbar__font-login col-12 navbar__hipervinculo navbar__padding_zero"><i class="fas fa-map-marker-alt"></i> <?=InPage::__('home_movil_menu_5','Ubicanos')?></a>
                  <hr class="w-100 my-0">
            </div>  </div>
     
    </nav>
    <!--/.Navbar-->
